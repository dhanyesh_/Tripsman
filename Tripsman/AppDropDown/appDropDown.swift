//
//  appDropDown.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 08/11/23.
//

import Foundation
import SwiftUI

struct DropDownMenuWithFixedTitile<T:Hashable>:View {
    let optionList:[T]
    @Binding var selecteOption:T?
    let label: (T) -> String
    let  header:String
    let onTap: (T) -> Void
    var body: some View {
        Menu {
            ForEach(optionList,id: \.self)
            {
                option in
                Button{
                  if(selecteOption==option)
                    {
                      selecteOption=nil
                  }
                    else
                    {
                        onTap(option)
                    }

                }
            label:
                {
                    Text(label(option))
                    if(selecteOption==option)
                    {
                        Image(systemName: "checkmark.circle")
                    }
                }
            }
            
                } label: {
                    Text(header)
                        .font(.robotoRegular(size: 16))
                        .foregroundColor(.appBlack34)
                    Spacer()
                    Image(AppIcons.arrowDown)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 10)
                }
                .padding(.horizontal,10)
                .padding(.vertical,5)
                .rounderBorder(cornerRadius: 5)
    }
}

struct DropDownMenu<T:Hashable>:View {
    let optionList:[T]
    @Binding var selecteOption:T?
    let label: (T?) -> String
    let  header:String
    let onTap: (T) -> Void
    var body: some View {
        VStack(alignment: .leading){
            Text(header)
                .font(.robotoRegular(size: 16))
                .foregroundColor(.appBlack34)
            Menu {
                ForEach(optionList,id: \.self)
                {
                    option in
                    Button{
                      if(selecteOption==option)
                        {
                          selecteOption=nil
                      }
                        else
                        {
                          onTap(option)
                        }

                    }
                label:
                    {
                        Text(label(option))
                        if(selecteOption==option)
                        {
                            Image(systemName: "checkmark.circle")
                        }
                    }
                }
                
                    } label: {
                        Text(label(selecteOption))
                            .font(.robotoRegular(size: 16))
                            .foregroundColor(.appBlack34)
                        Spacer()
                        Image(AppIcons.arrowDown)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 10)
                    }
                    .padding(.horizontal,10)
                    .padding(.vertical,5)
                    .rounderBorder(cornerRadius: 5)
        }
    }
}

//
//  AppConstants.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation

enum Module
{
    case hotel
    case holidayPackage
    case activity
    case meetUps
}


class UserDefaultKeys
{
    static let token:String = "token"
    static let language:String = "Language"
    static let country:String = "Country"
    static let currencyCode:String = "currencyCode"
    static let userId:String = "UserId"
    static let userName:String = "userName"
    static let userEmail:String = "userEmail"
}

class ApiConstants
{
    static let secretKey:String="SecretKey"
    static let secretKeyValue:String="xecretKeywqejane"
    static let authorization:String="Authorization"
}


let genderList=["Male","Female","Other","Prefer not to say"]

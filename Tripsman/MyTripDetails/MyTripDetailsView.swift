//
//  myTripsView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 08/11/23.
//

import SwiftUI

struct MyTripsDetailsView: View {
     let module:Module
     let id:Int
    @StateObject var viewModel:MyTripsDetailsViewModel=MyTripsDetailsViewModel()
    var body: some View {
        ScrollView(){
            
            MyTripDetailsViewTopContainer(
                tripsStatus: viewModel.tripsStatus, 
                bookingNumber:viewModel.bookingNumber,
                bookingDate: viewModel.bookingDate,
                tripMessage: viewModel.tripMessage)
            Spacer()
                .frame(height: 20)
            
            switch(module)
            {
            case Module.hotel:
                MyTripsItemDetailsView(
                    itemImage: viewModel.imageUrl,
                    itemName: viewModel.hotelName,
                    itemSubText: viewModel.hotelAddress)
                
            case Module.holidayPackage:
                MyTripsItemDetailsView(
                    itemImage: viewModel.imageUrl,
                    itemName: viewModel.holidayPackageName,
                    itemSubText: viewModel.holidayPackageDescription)
                
            case Module.activity:
                MyTripsItemDetailsView(
                    itemImage: viewModel.imageUrl,
                    itemName: viewModel.activityName,
                    itemSubText: viewModel.activityDiscription)
                
            case Module.meetUps:
                MyTripsItemDetailsView(
                    itemImage: viewModel.imageUrl,
                    itemName: viewModel.meetUpName,
                    itemSubText: viewModel.meetUpDescription)
                
            }  
            Spacer()
                .frame(height: 30)
            
            switch(module)
            {
            case Module.hotel:MyTripsHotelBookingDetailsView(
                checkinDate: viewModel.checkInDate,
                checkOutDate: viewModel.checkOutdate,
                checkInTime: viewModel.checkInTime,
                checkOutTime: viewModel.checkOutTime, 
                durationOfStay: viewModel.durationOfStay,
                occupantsCount:viewModel.occupantsCount,
                roomType: viewModel.roomType,
                primaryGuestDeatils: viewModel.primaryGuest)
                
            case Module.holidayPackage:MyTripsHolidayPackageBookingDetailsView(
                startDate: viewModel.startDate,
                endDate: viewModel.endDate,
                durationOfStay: viewModel.durationOfStay,
                occupantsCount:viewModel.occupantsCount,
                primaryGuestDeatils: viewModel.primaryGuest)
                
            case Module.activity:MyTripsActivityBookingDetailsView(
                activityDate: viewModel.activityDate,
                totalMembers: viewModel.totalMembers,
                durationOfStay: viewModel.durationOfStay,
                occupantsCount:viewModel.occupantsCount,
                primaryGuestDeatils: viewModel.primaryGuest)
                
            case Module.meetUps:MyTripsMeetUpBookingDetailsView(
                meetupDate: viewModel.meetUpDate,
                totalMembers: viewModel.totalMembers ,
                durationOfStay: viewModel.durationOfStay,
                occupantsCount:viewModel.occupantsCount,
                primaryGuestDeatils: viewModel.primaryGuest)
            }
            
            
            Spacer()
                .frame(height: 40)
            
            ForEach(viewModel.amountDetails ?? [] ,id: \.self)
            {
                amount in
                    AmountDetailContainer(amount: amount)
            }
            Spacer()
                .frame(height: 20)
            
            if(viewModel.tripStatusValue == 0)
            {
                YellowButton(text: "Cancel Booking")
             
            }
            else if(viewModel.tripStatusValue == 1)
            {
                YellowButton(text: "Add Review")
            }
                
            
            
            
        }
        .padding(.horizontal,15)
        .task
    {
        await viewModel.getMyTipsDeatils(module: module, id: id)
    }
}
    }
       


struct AmountDetailContainer:View {
    let amount:AmountDetail?
 
    var body: some View {
        HStack
        {
            Text(amount?.label ?? "")
                .font(.robotoMedium(size: ((amount?.isTotalAmount ?? 0) == 0) ? 14 : 18))
            Spacer()
            Text("INR \(amount?.amount ?? 0)")
                .font(.robotoMedium(size:((amount?.isTotalAmount ?? 0) == 0) ? 14 : 18))
        }
        .padding(.top,((amount?.isTotalAmount ?? 0) == 0) ? 1 : 8)
    }
}


struct MyTripsHotelBookingDetailsView:View {
    let checkinDate:String
    let checkOutDate:String
    let checkInTime:String
    let checkOutTime:String
    let durationOfStay:String
    let occupantsCount:OccupantsCountForMyTripsDetails?
    let roomType:String
    let primaryGuestDeatils:PrimaryGuestDetailsForMyTripsDetails?
    var body: some View {
        HStack(alignment: .top){
           
            VStack(alignment: .leading, spacing: 15)
           {
               
               HStack{
                   MyTripsBookingDetailsListingItem(header: "Check-in", time: checkInTime, text: checkinDate,alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(header: "Check-Out", time: checkOutTime, text: checkOutDate,alignment: .trailing)
               }
               HStack{
                   MyTripsBookingDetailsListingItem(text: "\( (occupantsCount?.roomCount ?? 0)) Room for \(occupantsCount?.adultCount ?? 0) Adults\nand \((occupantsCount?.childCount ?? 0)) children", alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(durationOfStay: durationOfStay, text: roomType,alignment: .trailing)
               }
               
               MyTripsBookingDetailsListingItem(
                header: "Primary Guest",
                mobile: primaryGuestDeatils?.mobile ?? "",
                email: primaryGuestDeatils?.email ?? "",
                text:primaryGuestDeatils?.name ?? "",
                alignment: .leading)
            
           }
       }
            
    }
}

struct MyTripsActivityBookingDetailsView:View {
    let activityDate:String
    let totalMembers:Int
    let durationOfStay:String
    let occupantsCount:OccupantsCountForMyTripsDetails?
    let primaryGuestDeatils:PrimaryGuestDetailsForMyTripsDetails?
    var body: some View {
        HStack(alignment: .top){
           
            VStack(alignment: .leading, spacing: 15)
           {
               
               HStack{
                   MyTripsBookingDetailsListingItem(header: "Meetup Date", text: activityDate, alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(header: "Total Members", text: String(totalMembers),alignment: .trailing)
               }
               
               MyTripsBookingDetailsListingItem(
                header: "Primary Customer",
                mobile: primaryGuestDeatils?.mobile ?? "",
                email: primaryGuestDeatils?.email ?? "",
                text:"\(primaryGuestDeatils?.name ?? ""),\(primaryGuestDeatils?.gender ?? ""), \(primaryGuestDeatils?.age ?? 0) yrs",
                alignment: .leading)
            
           }
       }
            
    }
}
struct MyTripsMeetUpBookingDetailsView:View {
    let meetupDate:String
    let totalMembers:Int
    let durationOfStay:String
    let occupantsCount:OccupantsCountForMyTripsDetails?
    let primaryGuestDeatils:PrimaryGuestDetailsForMyTripsDetails?
    var body: some View {
        HStack(alignment: .top){
           
            VStack(alignment: .leading, spacing: 15)
           {
               
               HStack{
                   MyTripsBookingDetailsListingItem(header: "Meetup Date", text: meetupDate, alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(header: "Total Members", text: String(totalMembers),alignment: .trailing)
               }
               
               MyTripsBookingDetailsListingItem(
                header: "Primary Customer",
                mobile: primaryGuestDeatils?.mobile ?? "",
                email: primaryGuestDeatils?.email ?? "",
                text:"\(primaryGuestDeatils?.name ?? ""),\(primaryGuestDeatils?.gender ?? ""), \(primaryGuestDeatils?.age ?? 0) yrs",
                alignment: .leading)
            
           }
       }
            
    }
}
struct MyTripsHolidayPackageBookingDetailsView:View {
    let startDate:String
    let endDate:String
    let durationOfStay:String
    let occupantsCount:OccupantsCountForMyTripsDetails?
    let primaryGuestDeatils:PrimaryGuestDetailsForMyTripsDetails?
    var body: some View {
        HStack(alignment: .top){
           
            VStack(alignment: .leading, spacing: 15)
           {
               
               HStack{
                   MyTripsBookingDetailsListingItem(header: "Start Date", text: startDate, alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(header: "End Date", text: endDate,alignment: .trailing)
               }
               HStack{
                   MyTripsBookingDetailsListingItem(header:"Travellers",text: "\(occupantsCount?.adultCount ?? 0) Adults and \((occupantsCount?.childCount ?? 0)) children", alignment: .leading)
                   Spacer()
                   MyTripsBookingDetailsListingItem(header: "Duration", text: durationOfStay,alignment: .trailing)
               }
               
               MyTripsBookingDetailsListingItem(
                header: "Primary Guest",
                mobile: primaryGuestDeatils?.mobile ?? "",
                email: primaryGuestDeatils?.email ?? "",
                text:primaryGuestDeatils?.name ?? "",
                alignment: .leading)
            
           }
       }
            
    }
}







struct MyTripsBookingDetailsListingItem:View {
    let header:String?
    let time:String?
    let email:String?
    let mobile:String?
    let durationOfStay:String?
    let text:String
    let alignment:HorizontalAlignment
    init(header: String?=nil, time: String?=nil,mobile: String?=nil, durationOfStay: String?=nil,email:String?=nil, text: String, alignment: HorizontalAlignment) {
        self.header = header
        self.time = time
        self.email = email
        self.durationOfStay = durationOfStay
        self.mobile = mobile
        self.text = text
        self.alignment = alignment
    }
    var body: some View {
        
        VStack(alignment: alignment,spacing: 2)
        {
            if(header != nil)
            {
                Text(header!)
                    .font(.robotoRegular(size: 12))
                    .foregroundColor(.appGrey126)
            }
            Text(text)
                .font(.robotoMedium(size: 14))
                .foregroundColor(.appBlack34)
            if(time != nil)
            {
                Text(time!)
                    .font(.robotoRegular(size: 12))
                    .foregroundColor(.appGrey126)
            }   
            if(email != nil)
            {
                Text(email!)
                    .font(.robotoRegular(size: 12))
                    .foregroundColor(.appGrey126)
            } 
            if(mobile != nil)
            {
                Text(mobile!)
                    .font(.robotoRegular(size: 12))
                    .foregroundColor(.appGrey126)
            }
        }
    }
}  


struct MyTripsItemDetailsView:View {
    let itemImage:String
    let itemName:String
    let itemSubText:String
    var body: some View {
        HStack(alignment: .top){
            AppAsyncImage(url: itemImage,width: 170,height: 120, radius: 10)
            VStack(alignment: .leading, spacing: 5)
           {
               Text(itemName)
                   .font(.robotoMedium(size: 14))
                   .lineLimit(2)
               Text(itemSubText)
                   .font(.robotoRegular(size: 12))
                   .foregroundColor(.appGrey126)
           }
       }
            
    }
}



struct MyTripDetailsViewTopContainer:View {
    let tripsStatus:String
    let bookingNumber:String
    let bookingDate:String
    let tripMessage:String
    var body: some View {
        VStack(alignment: .leading,spacing: 3){
            Text(tripsStatus)
                .font(.robotoMedium(size: 16))
            Text("BOOKING ID - \(bookingNumber)")
                .font(.robotoMedium(size: 18))
                .foregroundColor(.appBlack77)
          
            Text("Booked On  \(bookingDate)")
                .font(.robotoMedium(size: 14))
                .foregroundColor(.appBlack77)
           
            Text(tripMessage)
                .font(.robotoRegular(size: 14))
                .foregroundColor(.appGrey126)
            Spacer()
                .frame(height: 5)
            BlueButton(text: "Download Invoice",width: UIScreen.main.bounds.width-50,radius: 5)
             
        }
        .padding(.horizontal,10)
        .padding(.vertical,8)
        .background(Color.appGrey227)
        .cornerRadius(13)
    }
}

#Preview {
    MyTripsDetailsView(
        module: .hotel, id: 1818
    )
}

//
//  myTripsModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 08/11/23.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myTripHotelDeatilsReposonseModel = try? JSONDecoder().decode(MyTripHotelDeatilsReposonseModel.self, from: jsonData)

import Foundation

// MARK: - MyTripHotelDeatilsReposonseModel
struct MyTripHotelDeatilsReposonseModel: Hashable, Codable{
    let data: MyTripHotelDeatils?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct MyTripHotelDeatils: Hashable, Codable {
    let bookingNo: String?
    let bookingDate: String?
    let totalAmount: Int?
    let bookingFrom: String?
    let bookingTo: String?
    let customerCode: String?
    let customerID: String?
    let customerName: String?
    let totalCharge: Int?
    let tax: Int?
    let serviceCharge: Int?
    let discount: Int?
    let totalPrice: Int?
    let totalGuest: Int?
    let primaryGuest: String?
    let contactNo: String?
    let emailId: String?
    let gender: String?
    let age: Int?
    let bookingId: Int?
    let reviewId: Int?
    let rating: Int?
    let hotelId: Int?
    let roomID: Int?
    let isCancelled: Int?
    let imageURL: String?
    let hotelName: String?
    let tripStatusValue: Int?
    let tripStatus: String?
    let roomCount: Int?
    let adultCount: Int?
    let childCount: Int?
    let tripMessage: String?
    let cancellationPolicy: String?
    let pointApplied: Int?
    let roomDetails: [RoomDetail]?
    let hotelDetails: HotelDetails?
    let hotelGuests: [HotelGuest]?
    let amountDetails: [AmountDetail]?
    let couponDetails: CouponDetails?
}

// MARK: - AmountDetail
struct AmountDetail: Hashable, Codable {
    let amount: Double?
    let label: String?
    let isTotalAmount: Int?
}
struct Amountdetail: Hashable, Codable {
    let amount: Double?
    let label: String?
    let isTotalAmount: Int?
}

// MARK: - CouponDetails
struct CouponDetails: Hashable, Codable {
    let couponName: String?
    let couponCode: String?
    let applicableModule: String?
    let validFrom: String?
    let validTo: String?
    let description: String?
    let minOrderValue: Int?
    let discountType: Int?
    let discountAmount: Int?
    let status: Int?
    let couponID: Int?
}

// MARK: - HotelDetails
struct HotelDetails: Hashable, Codable {
    let hotelID: Int?
    let hotelName: String?
    let address: String?
    let latitude: String?
    let longitude: String?
    let email: String?
    let phone: String?
    let description: String?
    let shortDescription: String?
    let propertyRules: String?
    let starRating: String?
    let checkInTime: String?
    let checkOutTime: String?
    let termsAndCondition: String?
    let serviceChargeType: String?
    let status: Int?
}

// MARK: - HotelGuest
struct HotelGuest: Hashable, Codable {
    let guestName: String?
    let contactNo: String?
    let email: String?
    let age: String?
    let gender: String?
    let bookingID: Int?
    let isPrimary: Int?
}

// MARK: - RoomDetail
struct RoomDetail: Hashable, Codable {
    let roomID: Int?
    let hotelID: Int?
    let roomCode: String?
    let roomType: String?
    let roomDescription: String?
    let roomPrice: Int?
    let serviceChargeValue: Int?
    let roomStatus: Int?
    let isDeleted: Int?
    let roomCount: Int?
    let bookingID: Int?
}


struct PrimaryGuestDetailsForMyTripsDetails
{
    var name:String
    var age:Int
    var email:String
    var mobile:String
    var gender:String
}

struct OccupantsCountForMyTripsDetails
{
    var adultCount:Int
    var roomCount:Int
    var childCount:Int
}



// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myTripHolidayPackageDeatilsReposonseModel = try? JSONDecoder().decode(MyTripHolidayPackageDeatilsReposonseModel.self, from: jsonData)

import Foundation

// MARK: - MyTripHolidayPackageDeatilsReposonseModel
struct MyTripHolidayPackageDeatilsReposonseModel: Hashable,Codable {
    let totalRecords: Int?
    let data: MyTripHolidayPackageDeatils?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct MyTripHolidayPackageDeatils: Hashable,Codable {
    let bookingNo: String?
    let bookingDate: String?
    let totalAmount: Int?
    let bookingFrom: String?
    let bookingTo: String?
    let customerCode: String?
    let customerName: String?
    let customerID: Int?
    let totalCharge: Int?
    let tax: Int?
    let serviceCharge: Int?
    let discount: Int?
    let totalPrice: Int?
    let totalGuest: Int?
    let primaryGuest: String?
    let contactNo: String?
    let emailId: String?
    let gender: String?
    let age: Int?
    let dataBookingID: Int?
    let bookingId: Int?
    let packageID: Int?
    let isCancelled: Int?
    let tripStatusValue: Int?
    let tripStatus: String?
    let tripMessage: String?
    let adultCount: Int?
    let childCount: Int?
    let imageURL: String?
    let paymentStatus: Int?
    let packagedetails: [Packagedetail]?
    let packageguest: [Packageguest]?
    let amountdetails: [Amountdetail]?
    let holidayVendor: HolidayVendor?
}

// MARK: - HolidayVendor
struct HolidayVendor: Hashable,Codable {
    let holidayID: Int?
    let vendorCode: String?
    let vendorName: String?
    let vendorAddress: String?
    let vendorDescription: String?
    let vendorStatus: Int?
    let vendorMobile: String?
    let vendorEmail: String?
    let serviceChargeType: String?
    let serviceChargeValue: Int?
    let vendorImage: String?
}

// MARK: - Packagedetail
struct Packagedetail: Hashable,Codable {
    let packageID: Int?
    let packageCode: String?
    let packageName: String?
    let countryID: Int?
    let shortDescription: String?
    let policies: String?
    let duration: String?
    let amount: Int?
    let count: Int?
    let costPerPerson: Int?
    let status: Int?
}

// MARK: - Packageguest
struct Packageguest: Hashable,Codable {
    let guestName: String?
    let contactNo: String?
    let email: String?
    let age: String?
    let gender: String?
    let bookingID: Int?
    let isPrimary: Int?
}


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myTripActivityDeatilsReposonseModel = try? JSONDecoder().decode(MyTripActivityDeatilsReposonseModel.self, from: jsonData)


// MARK: - MyTripActivityDeatilsReposonseModel
struct MyTripActivityDeatilsReposonseModel: Hashable, Codable {
    let totalRecords: Int?
    let data: MyTripActivityDeatils?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct MyTripActivityDeatils: Hashable ,Codable{
    let bookingNo: String?
    let bookingDate: String?
    let totalAmount: Int?
    let bookingFrom: String?
    let bookingTo: String?
    let customerId: Int?
    let customerCode: String?
    let customerName: String?
   
    let totalCharge: Int?
    let tax: Int?
    let serviceCharge: Int?
    let discount: Int?
    let totalPrice: Int?
    let totalGuest: Int?
    let primaryGuest: String?
    let contactNo: String?
    let emailId: String?
    let gender: String?
    let age: Int?
    let dataBookingId: Int?
    let bookingId: Int?
    let activityId: Int?
    let status: Int?
    let isCancelled: Int?
    let tripStatusValue: Int?
    let tripStatus: String?
    let tripMessage: String?
    let activitydetails: [Activitydetail]?
    let activityguest: [Activityguest]?
    let amountdetails: [Amountdetail]?
}

// MARK: - Activitydetail
struct Activitydetail: Hashable, Codable {
    let activityId: Int?
    let activityName: String?
    let activityCode: String?
    let activityImage: String?
    let countryId: String?
    let countryName: String?
    let shortDescription: String?
    let overview: String?
    let duration: String?
    let termsandConditions: String?
    let costPerPerson: Int?
    let status: Int?
}

// MARK: - Activityguest
struct Activityguest: Hashable, Codable {
    let id: Int?
    let guestName: String?
    let contactNo: String?
    let emailId: String?
    let age: String?
    let gender: String?
    let bookingId: Int?
    let status: Int?
    let isPrimary: Int?
}


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myTripMeetUpDeatilsReposonseModel = try? JSONDecoder().decode(MyTripMeetUpDeatilsReposonseModel.self, from: jsonData)

import Foundation

// MARK: - MyTripMeetUpDeatilsReposonseModel
struct MyTripMeetUpDeatilsReposonseModel: Hashable,Codable {
    let totalRecords: Int?
    let data: MyTripMeetUpDeatils?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct MyTripMeetUpDeatils: Hashable,Codable {
    let bookingNo: String?
    let bookingDate: String?
    let totalAmount: Int?
    let customerCode: String?
    let customerId: String?
    let customerName: String?
    let totalCharge: Int?
    let tax: Int?
    let serviceCharge: Int?
    let discount: Int?
    let totalPrice: Int?
    let totalGuest: Int?
    let primaryGuest: String?
    let contactNo: String?
    let emailId: String?
    let gender: String?
    let age: Int?
    let bookingId: Int?
    let meetupId: Int?
    let isCancelled: Int?
    let imageUrl: String?
    let meetupName: String?
    let adultCount: Int?
    let tripStatusValue: Int?
    let tripStatus: String?
    let tripMessage: String?
    let city: String?
    let meetupGuests: [MeetupGuest]?
    let meetupDetails: MeetupDetails?
    let amountDetails: [AmountDetail]?
}

// MARK: - MeetupDetails
struct MeetupDetails: Hashable,Codable {
    let meetupId: Int?
    let meetupName: String?
    let meetupDate: String?
    let countryId: String?
    let countryName: String?
    let shortDescription: String?
    let termsandConditions: String?
    let costPerPerson: Int?
    let status: Int?
}

// MARK: - MeetupGuest
struct MeetupGuest: Hashable,Codable {
    let guestName: String?
    let contactNo: String?
    let email: String?
    let age: String?
    let gender: String?
    let bookingId: Int?
    let isPrimary: Int?
}

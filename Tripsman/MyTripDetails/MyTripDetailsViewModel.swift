//
//  myTripsViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 08/11/23.
//

import Foundation
class MyTripsDetailsViewModel:ObservableObject
       {
    @Published var bookingNumber=""
    @Published var imageUrl=""
    @Published var tripsStatus=""
    @Published var tripStatusValue=0
    @Published var bookingDate=""
    @Published var tripMessage=""
   
   

    @Published var amountDetails:[AmountDetail]?

    @Published var primaryGuest:PrimaryGuestDetailsForMyTripsDetails?
    
    @Published var durationOfStay=""
    @Published var totalMembers:Int = 0
    @Published var occupantsCount:OccupantsCountForMyTripsDetails?
    
    
    func getMyTipsDeatils(module:Module,id:Int)async
    {
        switch(module)
    {
        case Module.hotel:          await getMyTipsDeatilsHotel(id: id)
        case Module.meetUps:        await getMyTipsDeatilsMeetUp(id: id)
        case Module.activity:       await getMyTipsDeatilsActivity(id: id)
        case Module.holidayPackage: await getMyTipsDeatilsHolidayPackage(id: id)
    }
    }
    
    @Published var roomType=""
    @Published var hotelName=""
    @Published var checkInDate=""
    @Published var checkInTime=""
    @Published var checkOutTime=""
    @Published var hotelAddress=""
    @Published var checkOutdate=""


    
    func getMyTipsDeatilsHotel(id:Int)async
    {
      do
          {
              let response:MyTripHotelDeatilsReposonseModel = try await ApiServices.getCustomerHotelBookingById(queryItems: ["BookingId" : String(id)])
              let tripDetails=response.data
              tripsStatus = tripDetails?.tripStatus ?? ""
              bookingNumber = tripDetails?.bookingNo ?? ""
              tripStatusValue = tripDetails?.tripStatusValue ?? 0
            
              tripMessage = tripDetails?.tripMessage ?? ""
              hotelName = tripDetails?.hotelDetails?.hotelName ?? ""
              hotelAddress = tripDetails?.hotelDetails?.address ?? ""
              hotelAddress = tripDetails?.hotelDetails?.address ?? ""
              checkInDate = tripDetails?.bookingFrom?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              checkOutdate = tripDetails?.bookingTo?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              bookingDate = tripDetails?.bookingDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
           
              occupantsCount=OccupantsCountForMyTripsDetails(adultCount: tripDetails?.adultCount ?? 0, roomCount: tripDetails?.roomCount ?? 0, childCount: tripDetails?.childCount ?? 0)
              checkOutTime=tripDetails?.hotelDetails?.checkOutTime ?? ""
              checkInTime=tripDetails?.hotelDetails?.checkInTime ?? ""
              imageUrl = tripDetails?.imageURL ?? ""
              roomType=tripDetails?.roomDetails?.first?.roomType ?? ""
              primaryGuest=PrimaryGuestDetailsForMyTripsDetails(
                name:tripDetails?.primaryGuest ?? "",
                age:tripDetails?.age ?? 0,
                email:tripDetails?.emailId ?? "",
                mobile:tripDetails?.contactNo ?? "",
                gender:tripDetails?.gender ?? ""
              )
              amountDetails = tripDetails?.amountDetails ?? []
          }
        catch
        {
            print(error)
        }
    }
    
    @Published var holidayPackageName:String = ""
    @Published var holidayPackageDescription:String = ""
    @Published var startDate:String = ""
    @Published var endDate:String = ""

    func getMyTipsDeatilsHolidayPackage(id:Int)async
    {
      do
          {
              let response: MyTripHolidayPackageDeatilsReposonseModel = try await ApiServices.getMyTipsDeatilsHolidayPackage(queryItems: ["BookingId" : String(id)])
              let tripDetails=response.data
              tripsStatus = tripDetails?.tripStatus ?? ""
              bookingNumber = tripDetails?.bookingNo ?? ""
              bookingDate = tripDetails?.bookingDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              tripMessage = tripDetails?.tripMessage ?? ""
              tripStatusValue = tripDetails?.tripStatusValue ?? 0
              
              holidayPackageName = tripDetails?.packagedetails?.first?.packageName ?? ""
              holidayPackageDescription = tripDetails?.packagedetails?.first?.shortDescription ?? ""
              occupantsCount=OccupantsCountForMyTripsDetails(adultCount: tripDetails?.adultCount ?? 0, roomCount: 0, childCount: tripDetails?.childCount ?? 0)
              durationOfStay=tripDetails?.packagedetails?.first?.duration ?? ""
              durationOfStay=tripDetails?.packagedetails?.first?.duration ?? ""
              startDate=tripDetails?.bookingFrom?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              endDate=tripDetails?.bookingTo?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              primaryGuest=PrimaryGuestDetailsForMyTripsDetails(
                name:tripDetails?.primaryGuest ?? "",
                age:tripDetails?.age ?? 0,
                email:tripDetails?.emailId ?? "",
                mobile:tripDetails?.contactNo ?? "",
                gender:tripDetails?.gender ?? ""
              )
             amountDetails=tripDetails?.amountdetails?.map{AmountDetail(amount: ($0.amount), label: $0.label, isTotalAmount: $0.isTotalAmount)}
              
          }
        catch
        {
            print(error)
        }
    }
    
    
    @Published var activityName:String = ""
    @Published var activityDiscription:String = ""
    @Published var activityDate:String = ""
   
    
    func getMyTipsDeatilsActivity(id:Int)async
    {
      do
          {
              let response:MyTripActivityDeatilsReposonseModel = try await ApiServices.getMyTipsDeatilsActivity(queryItems: ["BookingId" : String(id)])
              let tripDetails=response.data
              bookingNumber = tripDetails?.bookingNo ?? ""
              tripsStatus = tripDetails?.tripStatus ?? ""
              tripMessage = tripDetails?.tripMessage ?? ""
              tripStatusValue = tripDetails?.tripStatusValue ?? 0
              bookingDate = tripDetails?.bookingDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
           
              
              activityName=tripDetails?.activitydetails?.first?.activityName ?? ""
              activityDiscription=tripDetails?.activitydetails?.first?.shortDescription ?? ""
              
              activityDate = tripDetails?.bookingDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              totalMembers=tripDetails?.totalGuest ?? 0
              primaryGuest=PrimaryGuestDetailsForMyTripsDetails(
                name:tripDetails?.primaryGuest ?? "",
                age:tripDetails?.age ?? 0,
                email:tripDetails?.emailId ?? "",
                mobile:tripDetails?.contactNo ?? "",
                gender:tripDetails?.gender ?? ""
              )
              amountDetails=tripDetails?.amountdetails?.map{AmountDetail(amount: $0.amount, label: $0.label, isTotalAmount: $0.isTotalAmount)}
          }
        catch
        {
            print(error)
        }
    }
    
    @Published var meetUpName:String = ""
    @Published var meetUpDescription:String = ""
    @Published var meetUpDate:String = ""
    
    func getMyTipsDeatilsMeetUp(id:Int)async
    {
      do
          {
              let response:MyTripMeetUpDeatilsReposonseModel = try await ApiServices.getMyTipsDeatilsMeetUp(queryItems: ["BookingId" : String(id)])
              let tripDetails=response.data
              
              tripsStatus = tripDetails?.tripStatus ?? ""
              bookingNumber = tripDetails?.bookingNo ?? ""
              bookingDate = tripDetails?.bookingDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              tripMessage = tripDetails?.tripMessage ?? ""
              tripStatusValue = tripDetails?.tripStatusValue ?? 0
              meetUpName = tripDetails?.meetupDetails?.meetupName ?? ""
              meetUpDescription = tripDetails?.meetupDetails?.shortDescription ?? ""
              meetUpDescription = tripDetails?.meetupDetails?.shortDescription ?? ""
              meetUpDate = tripDetails?.meetupDetails?.meetupDate?.convertISODateTo(fromat: "E dd MMM yyy") ?? ""
              totalMembers = tripDetails?.totalGuest ?? 0
           
              primaryGuest=PrimaryGuestDetailsForMyTripsDetails(
                name:tripDetails?.primaryGuest ?? "",
                age:tripDetails?.age ?? 0,
                email:tripDetails?.emailId ?? "",
                mobile:tripDetails?.contactNo ?? "",
                gender:tripDetails?.gender ?? ""
              )
              amountDetails = tripDetails?.amountDetails ?? []
          }
        catch
        {
            print(error)
        }
    }
}

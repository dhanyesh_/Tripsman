//
//  homeScreenModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let roomDetailsResponseModel = try? JSONDecoder().decode(RoomDetailsResponseModel.self, from: jsonData)

import Foundation


struct GetHomePageBannersResponseModel: Codable, Hashable  {
    let totalRecords: Int?
    let data: [Banners]?
    let status: Int?
    let message: String?
}


struct Banners: Codable, Hashable {
    let id: Int?
    let url: String?
}


struct HomePageCategoryModel
{
    let image:String
    let text:String
}

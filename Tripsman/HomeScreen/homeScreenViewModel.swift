import Foundation
import UIKit


class HomePageViewModel: ObservableObject
{
    @Published var homeCategoryMeetups :HomePageCategoryModel=HomePageCategoryModel(image: AppImages.homeCategoryMeetups, text: "Meetups")
    @Published var homeCategoryHotels :HomePageCategoryModel=HomePageCategoryModel(image: AppImages.homeCategoryHotels, text: "Hotels")
    @Published var homeCategoryHolidays :HomePageCategoryModel=HomePageCategoryModel(image: AppImages.homeCategoryHoliday, text: "Holidays packages")
    @Published var homeCategoryActivites :HomePageCategoryModel=HomePageCategoryModel(image: AppImages.homeCategoryActivites, text: "Activites")
    
   
    
    @Published var banners:[Banners]=[]
   
    func getHomePageData()async
    {
        do
        {
     
            let response:GetHomePageBannersResponseModel=try await ApiServices.getHomePageBanners()

                self.banners=response.data ?? []
        }
        catch
        {
            print(error);
        }
    }
}

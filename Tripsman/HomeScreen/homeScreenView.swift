//
//  homeScreenView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import SwiftUI

struct HomeScreenView: View {
    @StateObject var viewModel:HomePageViewModel=HomePageViewModel()
    var body: some View {
     
       
           
                ScrollView()
                {
                    VStack{
                       
                        if(!viewModel.banners.isEmpty)
                        {
                            TopBanner(banners: viewModel.banners)
                            
                        }
                        Spacer()
                            .frame(height: 20)
                        LazyVGrid(columns:[
                            GridItem(.flexible(),spacing: 10),
                            GridItem(.flexible())
                        ],spacing:10 )
                       {
                          
                               NavigationLink(destination: HotelListingView(),
                          
                                              label:{
                               HomePageCategoryContainer(category: viewModel.homeCategoryHotels)
                           })
                           NavigationLink(destination: HolidayPackageListingView(),
                          
                                              label:{
                               HomePageCategoryContainer(category: viewModel.homeCategoryHolidays)
                           })
                           NavigationLink(destination: ActivityListingView(),
                          
                                              label:{
                               HomePageCategoryContainer(category: viewModel.homeCategoryActivites)
                           })
                          
                           NavigationLink(destination: MeetUpListingView(),
                          
                                              label:{
                               HomePageCategoryContainer(category: viewModel.homeCategoryMeetups)
                           })
                           
                           
                           
                           
                        }
                        
                    }
                    
                    .task {
                        await viewModel.getHomePageData()
                    }
                }
                .padding(.horizontal,15)
           
            
        }
    }


struct HomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreenView()
    }
}






struct TopBanner: View {
    var banners:[Banners]
    var body: some View {
        TabView(){
            ForEach(banners,id: \.self)
            {
                banner in
                AppAsyncImage(url: banner.url ?? "",width: UIScreen.main.bounds.width - 30, radius: 10)
            }
            .scrollDisabled(false)
        }
        .tabViewStyle(PageTabViewStyle())
        .frame(height: 200)
    }
}

struct HomePageCategoryContainer: View {
    let category:HomePageCategoryModel
    var body: some View {
        ZStack(alignment: .bottom){
            Image(category.image)
                .resizable()
                .scaledToFill()
            Text(category.text)
                .foregroundColor(.appWhite)
                .font(.robotoBold(size:15))
                .padding(.vertical,5)
                .frame(maxWidth: .infinity)
                .background(Color.appBlue.gradient)
                .cornerRadius(10)
        }
        .cornerRadius(10)
    }
}


//
//  AppAsyncImage.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 02/11/23.
//

import SwiftUI

struct AppAsyncImage: View {
    let url :String
    let width: Double?
    let height: Double?
    let radius: Double?
    init(url: String, width: Double?=nil, height: Double?=nil, radius: Double?=nil) {
        self.url = url
        self.width = width
        self.height = height
        self.radius = radius
    }
    
    var body: some View {
        AsyncImage(url:URL(string:(url)))
        {
            image in
            image.resizable()
                .scaledToFill()
                .frame(width:width ?? 200,height: height ?? 200)
                .cornerRadius(radius ?? 0)
         
              
               
        }
    placeholder:
        {
            
            Text("T")
                .frame(width: width ?? 200 ,height: height ?? 200)
                .foregroundColor(.gray)
                .overlay(RoundedRectangle(cornerRadius: radius ?? 0).stroke())
                
                
            
//            ProgressView()
//                .progressViewStyle(.circular)
        }
    }
}

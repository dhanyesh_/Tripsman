//
//  meetUpViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import Foundation
import Foundation
class MeetUpListingViewModel:ObservableObject
{
    @Published var mettUpList:[MeetUpForListing]=[]
    @Published var filters:[FilterItems]=[]
    @Published var sortBy:[SortbyItems]=[]
    @Published var country:String="Select Country"
    @Published var city:String="Select City"
    @Published var selectedSortBy:SortbyItems?
    @Published var  showFilters:Bool=false
    @Published var  showExpandedFilter:Bool=false
    
    func getMeetUpList()async{
        do
        {
            
            var body:[String:Any]=["budgetTo": 100000, "sortBy": "", "Currency": "INR", "MeetupCity": "", "budgetFrom": 0, "offset": 0, "recordCount": 20, "Country": "IND", "Language": "en", "meetupFilters": [:], "MeetupCountry": "DEU"]
            var response:MeetUpListingModel=try await ApiServices.getMeetUpListing(body: body)
            mettUpList=response.data ?? []
        }
        catch
        {
            print(error)
        }
    }
    
    func getMeetUpFilters()async
    {
        do
        {
            let response:FilterResponseModel = try await ApiServices.getMeetUpFilter(queryItems: [UserDefaultKeys.language:UserDefaults.standard.string(forKey:UserDefaultKeys.language) ?? ""])
            
            filters=response.data?.filters ?? []
            sortBy=response.data?.sortby ?? []
        }
        catch
        {
            print(error)
        }
    }
}

//
//  MeetUpView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import SwiftUI



struct MeetUpListingView: View {
    @StateObject var viewModel:MeetUpListingViewModel=MeetUpListingViewModel()
    var body: some View {
        ScrollView{
            MeetUpFilterView(viewModel: viewModel)
            LazyVGrid(columns: [
                GridItem(.flexible(),spacing: 10),
                GridItem(.flexible())
            ],spacing: 10)
            {
                ForEach(viewModel.mettUpList,id: \.self)
                {
                    meetUp in
                    MeetUpListingItemView(meetUp: meetUp)
                }
            }
            .padding(.horizontal,15)
        }
        .task {
            //            await viewModel.getMeetUpList()
            await viewModel.getMeetUpFilters()
        }
    }
}



struct MeetUpFilterView: View {
    @ObservedObject var viewModel: MeetUpListingViewModel
    
    var body: some View {
        VStack
        {
            HStack{
                HStack{
                    VStack(alignment: .leading)
                    {
                        Text("\(viewModel.country) | \(viewModel.city)")
                            .font(.robotoRegular(size: 12))
                            .foregroundColor(.appBlack77)
                    }
                    Spacer()
                    EditButton()
                    
                }
                .padding(.all,5)
                .rounderBorder(cornerRadius: 5)
                .onTapGesture {
                    viewModel.showExpandedFilter = true
                }
                SearchButton()
            }
            if(viewModel.showExpandedFilter){
                VStack{
                    HStack{
                        Text(viewModel.country)
                            .font(.robotoRegular(size: 12))
                        
                        Spacer()
                        Image(systemName:"globe.europe.africa.fill")
                    }
                    .foregroundColor(.appBlack77)
                    .padding(.all,8)
                    .rounderBorder(cornerRadius: 5)
                    Spacer()
                    HStack{
                        Text(viewModel.city)
                            .font(.robotoRegular(size: 12))
                        
                        Spacer()
                        Image(systemName:"globe.europe.africa.fill")
                    }
                    .foregroundColor(.appBlack77)
                    .padding(.all,8)
                    .rounderBorder(cornerRadius: 5)
                    
                    
                }
                HStack{
                    YellowButton(text: "Search",width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                    Spacer()
                    GreyButton(text: "Cancel", width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                }
            }
            
            else {
                HStack{
                    FilterByButton(bottomSheet: MeetUpBottomSheet(filters: viewModel.filters),presentationDetent: [.fraction(0.35)])
                    Spacer()
                    DropDownMenuWithFixedTitile(optionList:viewModel.sortBy , selecteOption: $viewModel.selectedSortBy, label: {
                        option in
                        option.name ?? ""
                    }, header: "Sort By",onTap:{
                        option in
                        print(option)
                        
                    })
                }
            }
        }
        .padding(.horizontal,10)
    }
}

struct MeetUpBottomSheet:View {
    let filters:[FilterItems]
    var body: some View {
        ScrollView{
            Spacer().frame(height: 30)
            ForEach(filters, id: \.self) { filter in
                VStack(alignment: .leading){
                    Text(filter.title ?? "")
                        .font(.robotoMedium(size: 16))
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    
                    FilterItemListing(valueList: filter.values ?? [])
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    Divider()
                }
                
            }
            Spacer().frame(height: 30)
            YellowButton(text: "Apply")
        }
        
    }
}


struct MeetUpListingItemView:View {
    let meetUp :MeetUpForListing
    var body: some View {
        VStack(alignment: .leading){
            AppAsyncImage(url: meetUp.meetupImages?.first?.imageUrl ?? "",width: UIScreen.main.bounds.width*0.43,height: 100,radius: 10)
                .padding(.bottom,5)
            
            
            Text(meetUp.meetupName ?? "")
                .font(.robotoMedium(size: 14))
            Text(meetUp.meetupDate ?? "")
                .font(.robotoRegular(size: 12))
                .lineLimit(1)
            
            Text(meetUp.shortDescription ?? "")
                .font(.robotoRegular(size: 10))
                .foregroundColor(.appBlack77)
                .lineLimit(2)
                .padding(.bottom,2)
            HStack(alignment: .bottom){
                PriceLabel(
                    amount:meetUp.costPerPerson ?? 0,
                    offerAmount:  meetUp.offerAmount ?? 0,
                    fontSize: 13,
                    reduceFontSizeBy: 4,
                    newLine: true
                )
                Spacer()
                TaxAndFee(amount: Int(meetUp.serviceCharge ?? 0), fontSize: 8)
                
                
            }
            
            
        }
        .padding(.horizontal,2)
        .padding(.top,3)
        .padding(.bottom,5)
        .frame(height:210)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 13))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)
        
    }
}


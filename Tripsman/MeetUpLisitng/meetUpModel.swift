//
//  meetUpModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//
import Foundation

// MARK: - MeetUpListingModel
struct MeetUpListingModel: Hashable, Codable{
    let totalRecords: Int?
    let data: [MeetUpForListing]?
    let status: Int?
    let message: String?
}

// MARK: - Datum
struct MeetUpForListing: Hashable, Codable {
    let meetupId: Int?
    let meetupName: String?
    let address: String?
    let meetupCode: String?
    let countryId: Int?
    let countryName: String?
    let shortDescription: String?
    let termsAndConditions: String?
    let details: String?
    let latitude: String?
    let longitude: String?
    let costPerPerson: Int?
    let offerAmount: Int?
    let serviceCharge: Int?
    let meetupDate: String?
    let meetupToDate: String?
    let meetupTime: String?
    let meetupType: String?
    let meetupStatus: String?
    let tripStatusValue: Int?
    let tripStatus: String?
    let city: String?
    let meetupImages: [MeetupImage]?
}

// MARK: - MeetupImage
struct MeetupImage: Hashable, Codable {
    let id: Int?
    let imageUrl: String?
    let isFeatured: Int?
    let meetupId: Int?
    let status: Int?
}



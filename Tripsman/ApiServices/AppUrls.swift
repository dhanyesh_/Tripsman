//
//  AppUrls.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation

class AppUrls
{
    static let baseUrl:String   = "https://tripsmanadmin.hexeam.in"
    
    static let login = "\(baseUrl)/api/account/login"
    
    
    static let wallet = "\(baseUrl)/api/CustomerCoupon/GetCustomerWalletList"
    static let myTrips = "\(baseUrl)/api/CustomerHotelBooking/GetCustomerBookingListAll"
    static let getCustomerWalletList = "\(baseUrl)/api/CustomerCoupon/GetCustomerWalletList"
    static let homePageBanners = "\(baseUrl)/api/CustomerBanner/GetCustomerBannerList?Type=home"
    static let getCountryList = "\(baseUrl)/api/Country/GetCountryList"
    static let getCustomerProfile = "\(baseUrl)/api/CustomerProfile/GetCustomerProfile"
   

    static let getCustomerHolidayBookingById = "\(baseUrl)/api/CustomerHoliday/GetCustomerHolidayBookingById"
    static let GetCustomerHotelBookingById = "\(baseUrl)/api/CustomerHotelBooking/GetCustomerHotelBookingById"
    static let getCustomerMeetupBookingListById = "\(baseUrl)/api/CustomerMeetup/GetCustomerMeetupBookingListById"
    static let getCustomerActivityBookingListById = "\(baseUrl)/api/CustomerActivity/GetCustomerActivityBookingListById"

    static let getCustomerHotelList = "\(baseUrl)/api/CustomerHotel/GetCustomerHotelList"
    static let getCustomerMeetupList = "\(baseUrl)/api/CustomerMeetup/GetCustomerMeetupList"
    static let getCustomerActivityList = "\(baseUrl)/api/CustomerActivity/GetCustomerActivityList"
    static let getCustomerHolidayPackageList = "\(baseUrl)/api/CustomerHoliday/GetCustomerHolidayPackageList"
    
    static let getCustomerHotelFilters = "\(baseUrl)/api/CustomerHotel/GetCustomerHoteFilterlList"
    static let getCustomerMeetupFilterlList = "\(baseUrl)/api/CustomerMeetup/GetCustomerMeetupFilterlList"
    static let getCustomerActivityFilterlList = "\(baseUrl)/api/CustomerActivity/GetCustomerActivityFilterlList"
    static let getCustomerHolidayPackageFilterList = "\(baseUrl)/api/CustomerHoliday/GetCustomerHolidayFilterlList"
  
}

//
//  ApiServices.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//


import Foundation
class ApiServices
{
    
    
    static func getToken()->String
    {
        UserDefaults.standard.setValue("INR",forKey: UserDefaultKeys.currencyCode)
        UserDefaults.standard.setValue("en",forKey: UserDefaultKeys.language)
        UserDefaults.standard.setValue("43d68ed8-0582-4a95-baf5-6459585e16f5",forKey: UserDefaultKeys.userId)
        
        UserDefaults.standard.setValue("", forKey: UserDefaultKeys.token)
//        UserDefaults.standard.setValue("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoie1wiSWRcIjpcIjQzZDY4ZWQ4LTA1ODItNGE5NS1iYWY1LTY0NTk1ODVlMTZmNVwiLFwiRW1haWxcIjpcImFyY2hhbmFjb2ZmaWNpYWxAZ21haWwuY29tXCIsXCJVc2VyVHlwZUlkXCI6MCxcIlVzZXJJZFwiOjAsXCJNb2RlbE5hbWVcIjpcIkN1c3RvbWVyXCJ9IiwiZXhwIjoxNzA3NjQ1ODA1LCJpc3MiOiJlY29tLmNvbSIsImF1ZCI6ImVjb20uY29tIn0.nYexQCZGiD0-jFPx-ZSiI00yFF_tLRwrB0GwjqbMKNI", forKey: UserDefaultKeys.token)
        
        return "Bearer \(UserDefaults.standard.string(forKey:UserDefaultKeys.token) ?? "")"
    }
    
    static func login(withBody queryItems:[String:String])async throws
    {
        var urlComponent=URLComponents(string:AppUrls.login )
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
        else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        
        request.httpMethod = HttpMethod.get
        let (data,response)=try await URLSession.shared.data(for: request)
        guard let response = response as? HTTPURLResponse
        else
        {
            throw ApiError.inVallidResponse
        }
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            
        }
        else
        {
            
        }
    }
    
    static func getHomePageBanners()async throws ->GetHomePageBannersResponseModel
    {
        let urlComponent=URLComponents(string: AppUrls.homePageBanners)
        guard let url = urlComponent?.url
        else
        {
            throw ApiError.invalidURL
        }
        var request=URLRequest(url:url)
        request.httpMethod = HttpMethod.get
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response) = try await URLSession.shared.data(for: request)
        print(url)
        print(String(decoding:data,as: UTF8.self))
        guard let  response = response as? HTTPURLResponse
        else
        {
            throw ApiError.inVallidResponse
        }
        if(response.statusCode == 200)
        {
            return try JSONDecoder().decode(GetHomePageBannersResponseModel.self, from: data)
        }
        else if(response.statusCode == 200)
        {
            throw ApiError.unAuthorized
        }
        else
        {
            throw ApiError.customError(message:String(decoding:data,as: UTF8.self))
        }
    }
    
    static func getMyTrips(withBody queryItems:[String:String])async throws -> MyTripsResponse
    {
        var urlComponent=URLComponents(string: AppUrls.myTrips)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
        else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod = HttpMethod.get
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        let (data,response)=try await URLSession.shared.data(for: request)
        guard let response = response as? HTTPURLResponse
        else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            return try JSONDecoder().decode(MyTripsResponse.self, from: data)
        }
        else if(response.statusCode == 200)
        {
            throw ApiError.unAuthorized
        }
        else
        {
            throw ApiError.customError(message:String(decoding:data,as: UTF8.self))
        }
        
    }
    
    static func getCustomerHotelBookingById(queryItems:[String:String])async throws ->MyTripHotelDeatilsReposonseModel
    {
        var urlComponent=URLComponents(string: AppUrls.GetCustomerHotelBookingById)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
        else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
        else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(MyTripHotelDeatilsReposonseModel.self, from: data)
        }
        else
        {
            throw ApiError.inVallidResponse
        }
    }
    
    static func getMyTipsDeatilsActivity(queryItems:[String:String])async throws ->MyTripActivityDeatilsReposonseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerActivityBookingListById)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(MyTripActivityDeatilsReposonseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getMyTipsDeatilsHolidayPackage(queryItems:[String:String])async throws ->MyTripHolidayPackageDeatilsReposonseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerHolidayBookingById)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(MyTripHolidayPackageDeatilsReposonseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getMyTipsDeatilsMeetUp(queryItems:[String:String])async throws ->MyTripMeetUpDeatilsReposonseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerMeetupBookingListById)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(MyTripMeetUpDeatilsReposonseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getHotelListing(body:[String:Any])async throws ->HotelListingModel
    {
        let urlComponent=URLComponents(string: AppUrls.getCustomerHotelList)
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.post
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        request.httpBody=try JSONSerialization.data(withJSONObject: body)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(HotelListingModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getHolidayPackageListListing(body:[String:Any])async throws ->HolidayPackageListingModel
    {
        let urlComponent=URLComponents(string: AppUrls.getCustomerHolidayPackageList)
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.post
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        request.httpBody=try JSONSerialization.data(withJSONObject: body)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(HolidayPackageListingModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getMeetUpListing(body:[String:Any])async throws ->MeetUpListingModel
    {
        let urlComponent=URLComponents(string: AppUrls.getCustomerMeetupList)
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.post
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        request.httpBody=try JSONSerialization.data(withJSONObject: body)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(MeetUpListingModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getActivityListingListing(body:[String:Any])async throws ->ActivityListingModel
    {
        let urlComponent=URLComponents(string: AppUrls.getCustomerActivityList)
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.post
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        request.httpBody=try JSONSerialization.data(withJSONObject: body)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(ActivityListingModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    
    static func getHotelFilter(queryItems:[String:String])async throws ->FilterResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerHotelFilters)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(FilterResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getMeetUpFilter(queryItems:[String:String])async throws ->FilterResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerMeetupFilterlList)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
       
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(FilterResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
        
    static func getActivityFilter(queryItems:[String:String])async throws ->FilterResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerActivityFilterlList)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(FilterResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
        
    static func getHolidayPackageFilter(queryItems:[String:String])async throws ->FilterResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerHolidayPackageFilterList)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(FilterResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }  
    
    static func getCustomerWalletDetails(queryItems:[String:String])async throws ->WalletResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerWalletList)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(WalletResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getCountryList()async throws ->GetCountryListResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCountryList)
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(GetCountryListResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
    
    static func getCustomerProfile(queryItems:[String:String])async throws ->GetProfileResponseModel
    {
        var urlComponent=URLComponents(string: AppUrls.getCustomerProfile)
        urlComponent?.queryItems=queryItems.map{URLQueryItem(name: $0.key, value: $0.value)}
        guard let url = urlComponent?.url
                else
        {
            throw ApiError.invalidURL
        }
        var request = URLRequest(url: url)
        request.httpMethod=HttpMethod.get
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(getToken(), forHTTPHeaderField: ApiConstants.authorization)
        request.setValue(ApiConstants.secretKeyValue, forHTTPHeaderField: ApiConstants.secretKey)
        let (data,response)=try await URLSession.shared.data(for: request)
        
        guard let response = response as? HTTPURLResponse
                else
        {
            throw ApiError.inVallidResponse
        }
        print(url)
        print(response.statusCode)
        print(String(decoding: data, as:UTF8.self))
        if(response.statusCode == 200)
        {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(GetProfileResponseModel.self, from: data)
        }
      else
        {
          throw ApiError.inVallidResponse
      }
    }
}



//
//  HttpMethods.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation
 class HttpMethod
{
     static let get:String  = "get"
     static let post:String = "POST"
}

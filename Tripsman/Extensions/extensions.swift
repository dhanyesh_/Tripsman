//
//  extensions.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation

extension String
{
    func convertISODateTo(fromat:String)->String
    {
        let dateFormater=DateFormatter()
        dateFormater.dateFormat = fromat
        if let date = ISO8601DateFormatter().date(from: self+"z")
        {
            return dateFormater.string(from: date)
        }
        else
        {
            return "invalid"
        }
    }
}


extension Date
{
    func formatTo(format:String) ->String
    {
        let dateFormater=DateFormatter()
        dateFormater.dateFormat = format
        return dateFormater.string(from: self)
    }
}

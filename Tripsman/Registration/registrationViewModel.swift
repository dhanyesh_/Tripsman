//
//  registrationViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation

class RegistrationViewMOdel:ObservableObject
{
    @Published var name:String = ""
    @Published var mobile:String = ""
    @Published var email:String = ""
    @Published var password:String = ""
    @Published var reTypePassword:String = ""
    @Published var showPassword:Bool = false
    @Published var showReTypePassword :Bool = false
}

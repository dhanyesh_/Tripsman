//
//  registrationView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation
import SwiftUI


struct RegistrationView: View
{
   @StateObject var viewModel:RegistrationViewMOdel=RegistrationViewMOdel()
    var body: some View
    {
        ZStack(alignment: .top)
        {
            Image(AppImages.registrartionBackGround)
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
            
            VStack {
                AppLogoWithText()
                    .padding(.vertical,30)
                VStack
                {
                    Text("REGISTER")
                        .font(.robotoMedium(size:23))
                    
                    AppTextField(text: $viewModel.name, placeHolder: "Name",width: UIScreen.main.bounds.width-60)
                    AppTextField(text: $viewModel.mobile, placeHolder: "Mobile",width: UIScreen.main.bounds.width-60)
                        .keyboardType(.numberPad)
                    AppTextField(text: $viewModel.email, placeHolder: "Email",width: UIScreen.main.bounds.width-60)
                    
                    PasswordTextField(text: $viewModel.password, isSecured: viewModel.showPassword, width: UIScreen.main.bounds.width-60, placeHolder: "Password" )
                        .underlineBorder()
                    PasswordTextField(text: $viewModel.reTypePassword, isSecured: viewModel.showReTypePassword, width: UIScreen.main.bounds.width-60, placeHolder: "Password" )
                        .underlineBorder()
                        .padding(.bottom,30)
                    
                    BlueButton(text:"Register", width:UIScreen.main.bounds.width-65)
                    
                    GreyButton(text:"Clear", width:UIScreen.main.bounds.width-65)
                }
                .padding(20)
                .frame(width: UIScreen.main.bounds.width-30)
                .background(Color.appYellow.opacity(0.75))
            .cornerRadius(10)
            }
        }
    }
    
}


struct RegistrationViewPreview: PreviewProvider
{
    static var previews: some View
    {
        RegistrationView()
        
    }
}



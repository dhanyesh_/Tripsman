//
//  AppLogoWithText.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import SwiftUI

struct AppLogoWithText: View {
    var body: some View {
        HStack(alignment: .bottom){
            Image(AppIcons.logo)
                .resizable()
                .scaledToFit()
                .frame(height: 60)
            Image(AppIcons.tripsmanText)
                .resizable()
                .scaledToFit()
                .frame(height: 30)
        }
    }
}

struct AppLogoWithText_Previews: PreviewProvider {
    static var previews: some View {
        AppLogoWithText()
    }
}

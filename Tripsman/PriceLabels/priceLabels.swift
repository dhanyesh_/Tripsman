//
//  priceLabel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 13/11/23.
//

import SwiftUI

struct PriceLabel: View {
    let amount:Int
    let offerAmount:Int?
    let multiplier:Int?
    let fontSize:CGFloat
    let reduceFontSizeBy:CGFloat?
    let newLine:Bool?
    init(amount: Int, offerAmount: Int?=nil, multiplier: Int?=nil, fontSize: CGFloat, reduceFontSizeBy: CGFloat?=nil,newLine: Bool?=nil) {
        self.amount = amount
        self.offerAmount = offerAmount ?? 0
        self.multiplier = multiplier ?? 1
        self.fontSize = fontSize
        self.reduceFontSizeBy = reduceFontSizeBy ?? 0
        self.newLine = newLine ?? false
    }
    var body: some View {
       if(offerAmount==0)
        {
        
               Text("\(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(amount)")
                   .font(.robotoMedium(size: fontSize))
           
       }
        else if((offerAmount ?? 0) < amount)
        {
         if( newLine ?? false)
       { VStack(alignment: .leading, spacing: 0){
                
                Text("\(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(amount)")
                    .font(.robotoMedium(size: fontSize-(reduceFontSizeBy ?? 0)))
                    .strikethrough()
                
                Text("\(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(offerAmount ?? 0)")
                    .font(.robotoMedium(size: fontSize))
               
        }}
            else{
                HStack(alignment: .bottom, spacing: 2){
                    
                    Text("\(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(amount)")
                        .font(.robotoRegular(size: fontSize-(reduceFontSizeBy ?? 0)))
                        .strikethrough()
                    
                    Text("\(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(offerAmount ?? 0)")
                        .font(.robotoMedium(size: fontSize))
                   
                }
            }
        }
        else
        {
         
                Text(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "")
                    .font(.robotoBold(size: fontSize))
                Text(String(amount))
                    .font(.robotoBold(size: fontSize))
        }
            
    }
}


struct TaxAndFee:View {
    let amount:Int
    let fontSize:CGFloat
    var body: some View {
        
               Text("+ \(UserDefaults.standard.string(forKey: UserDefaultKeys.currencyCode) ?? "") \(amount)\n taxes & fee per night")
            .foregroundColor(.appBlack77)
                   .font(.robotoRegular(size: fontSize))
                   .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                   .multilineTextAlignment(.trailing)
                   
    }
}


#Preview {
    PriceLabel(amount: 10000,offerAmount:4000, fontSize: 14,reduceFontSizeBy: 2)
}

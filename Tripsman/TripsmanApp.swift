//
//  TripsmanApp.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import SwiftUI

@main
struct TripsmanApp: App {
    var body: some Scene {
        WindowGroup {
          LandingScreenView()
        }
    }
}

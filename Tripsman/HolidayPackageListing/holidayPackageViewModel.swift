//
//  holidayPackageViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import Foundation
class HolidayPackageListingViewModel: ObservableObject
{
    @Published var country:String="All"
    @Published var showExpandedFilter:Bool=false
    @Published var date:Date=Date()
    @Published var adultCount:Int=1
    @Published var childCount:Int=0
    @Published var filters:[FilterItems]=[]
    @Published var sortBy:[SortbyItems]=[]
    @Published var selectedSortBy:SortbyItems?
    
    @Published var holidayPackageList:[HolidayPackageForListing]=[]
    func getHolidayPackagelList()async{
        do
        {
            var body:[String:Any] = ["sortBy":"",
                                     "Language":"en",
                                     "childCount":0,
                                     "offset":0,
                                     "countryName":"",
                                     "Currency":"INR",
                                     "maximumBudget":100000,
                                     "adultCount":2,
                                     "Country":"IND",
                                     "recordCount":20,
                                     "minimumBudget":0,
                                     "holidayFilters":[:]
            ]
            var response:HolidayPackageListingModel=try await ApiServices.getHolidayPackageListListing(body: body)
            holidayPackageList=response.data ?? []
        }
        catch
        {
            print(error)
        }
    }
    
    func getHolidayPackageFilters()async
    {
        do
        {
            let response:FilterResponseModel = try await ApiServices.getHolidayPackageFilter(queryItems: [UserDefaultKeys.language:UserDefaults.standard.string(forKey:UserDefaultKeys.language) ?? ""])
            filters=response.data?.filters ?? []
            sortBy=response.data?.sortby ?? []
        }
        catch
        {
            print(error)
        }
    }
}

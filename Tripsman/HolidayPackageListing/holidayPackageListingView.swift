//
//  holidatPackageListingView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import SwiftUI

#Preview
{
    HolidayPackageListingView()
}

struct HolidayPackageListingView: View {
    @StateObject var viewModel:HolidayPackageListingViewModel=HolidayPackageListingViewModel()
    var body: some View {
        HolidayPackageFilterView(viewModel: viewModel)
           ScrollView{
                LazyVGrid(columns: [
                    GridItem(.flexible(),spacing: 10),
                    GridItem(.flexible())
                ],spacing: 10)
                {
                    ForEach(viewModel.holidayPackageList,id: \.self)
                    {
                        holidayPackage in
                        HolidayPackageListingItemView(holidayPackage: holidayPackage)
                    }
                }
                .padding(.horizontal,15)
            }
           .task {
               await viewModel.getHolidayPackageFilters()
           }
    }
}

struct HolidayPackageFilterView: View {
    @ObservedObject var viewModel: HolidayPackageListingViewModel
    var body: some View {
        VStack
        {
            HStack{
                HStack{
                    VStack(alignment: .leading)
                    {
                        Text(viewModel.country)
                            .font(.robotoRegular(size: 14))
                            .foregroundColor(.appBlack34)
                        Text("\(viewModel.date.formatTo(format: "dd MMM yyyy")) | \(viewModel.adultCount+viewModel.childCount) Guests")
                            .font(.robotoRegular(size: 12))
                            .foregroundColor(.appBlack77)
                    }
                    Spacer()
                    EditButton()
                    
                }
                .padding(.all,5)
                .rounderBorder(cornerRadius: 5)
                .onTapGesture {
                    viewModel.showExpandedFilter = true
                }
                SearchButton()
            }
            if(viewModel.showExpandedFilter){
                VStack{
                    HStack{
                        Text(viewModel.country)
                            .font(.robotoRegular(size: 12))
                        
                        Spacer()
                        Image(systemName:"globe.europe.africa.fill")
                    }
                    .foregroundColor(.appBlack77)
                    .padding(.all,8)
                    .rounderBorder(cornerRadius: 5)
                   
                    DatePickerTextField(date: $viewModel.date, header: "StartDate")
                    HStack(spacing: 30)
                    {
                        AppStepper(header: "Adults", count: $viewModel.adultCount, minCount: 1)
                       
                        AppStepper(header: "Children", count: $viewModel.childCount, minCount: 0)
                        Spacer()
                    }

                }
                HStack{
                    YellowButton(text: "Search",width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                    Spacer()
                    GreyButton(text: "Cancel", width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                }
            }
            
            else {
                HStack{
                    FilterByButton(bottomSheet: MeetUpBottomSheet(filters: viewModel.filters),presentationDetent: [.fraction(0.3)])
                    Spacer()
                    DropDownMenuWithFixedTitile(optionList:viewModel.sortBy , selecteOption: $viewModel.selectedSortBy, label: {
                        option in
                        option.name ?? ""
                    }, header: "Sort By",onTap:{
                        option in
                        print(option)
                        
                    })
                }
            }
        }
        .padding(.horizontal,10)
    }
}

struct HolidayPackageBottomSheet:View {
    let filters:[FilterItems]
    var body: some View {
        ScrollView{
            Spacer().frame(height: 30)
            ForEach(filters, id: \.self) { filter in
                VStack(alignment: .leading){
                    Text(filter.title ?? "")
                        .font(.robotoMedium(size: 16))
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    
                    FilterItemListing(valueList: filter.values ?? [])
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    Divider()
                }
                
            }
            Spacer().frame(height: 30)
            YellowButton(text: "Apply")
        }
        
    }
}
                     
struct HolidayPackageListingItemView:View {
                    let holidayPackage :HolidayPackageForListing
                    var body: some View {
                        VStack(alignment: .leading){
                            AppAsyncImage(url: holidayPackage.holidayImage?.first?.imageUrl ?? "",width: UIScreen.main.bounds.width*0.43,height: 100,radius: 10)
                                .padding(.bottom,5)
                            
                            
                            Text(holidayPackage.packageName ?? "")
                                .padding(.bottom,1)
                                .font(.robotoMedium(size: 14))
                            Text(holidayPackage.duration ?? "")
                                .font(.robotoRegular(size: 12))
                                .lineLimit(1)
                            
                            Text(holidayPackage.shortDescription ?? "")
                                .font(.robotoRegular(size: 10))
                                .foregroundColor(.appBlack77)
                                .lineLimit(2)
                                .padding(.bottom,2)
                            HStack(alignment: .bottom){
                                PriceLabel(
                                    amount: holidayPackage.costPerPerson ?? 0,
                                    offerAmount:  holidayPackage.offerPrice ?? 0,
                                    fontSize: 13,
                                    reduceFontSizeBy: 4,
                                    newLine: true
                                )
                                Spacer()
                                TaxAndFee(amount: Int(holidayPackage.serviceCharge ?? 0), fontSize: 8)
                                
                                
                            }
                            
                            
                        }
                        .padding(.horizontal,2)
                        .padding(.top,3)
                        .padding(.bottom,5)
                        .frame(height:220)
                        .background(Color.appWhite)
                        
                        .clipShape(RoundedRectangle(cornerRadius: 13))
                        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
                        .padding(.horizontal,5)
                        
                    }
                }



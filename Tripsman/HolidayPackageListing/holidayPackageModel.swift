//
//  holidayPackageModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import Foundation


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let holidayPackageListingModel = try? JSONDecoder().decode(HolidayPackageListingModel.self, from: jsonData)

import Foundation

// MARK: - HolidayPackageListingModel
struct HolidayPackageListingModel: Hashable, Codable {
    let totalRecords: Int?
    let data: [HolidayPackageForListing]?
    let status: Int?
    let message: String?
}

// MARK: - Datum
struct HolidayPackageForListing: Hashable, Codable {
    let packageId: Int?
    let holidayId: Int?
    let packageCode: String?
    let packageName: String?
    let countryId: Int?
    let shortDescription: String?
    let policies: String?
    let duration: String?
    let durationDays: Int?
    let amount: Int?
    let count: Int?
    let packageCount: Int?
    let availableCount: Int?
    let costPerPerson: Int?
    let status: Int?
    let vendorId: Int?
    let countryName: String?
    let serviceCharge: Int?
    let packageType: Int?
    let packageTypeName: String?
    let isSponsored: Int?
    let ratingCount: Int?
    let userRating: Int?
    let offerPrice: Int?
    let vendorImage: String?
    let packageFrom: String?
    let packageTo: String?
    let bookingLimit: Int?
    let tripStatusValue: Int?
    let tripStatus: String?
    let holidayVendor: HolidayVendor?
    let holidayItinerary: [HolidayItinerary]?
    let holidayImage: [HolidayImage]?
}

// MARK: - HolidayImage
struct HolidayImage: Hashable, Codable {
    let id: Int?
    let packageId: Int?
    let imageUrl: String?
    let isFeatured: Int?
}

// MARK: - HolidayItinerary
struct HolidayItinerary: Hashable, Codable {
    let id: Int?
    let packageId: Int?
    let itineryName: String?
    let itineryDescription: String?
    let image: String?
    let day: String?
}


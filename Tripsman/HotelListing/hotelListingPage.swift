//
//  listing_page.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 10/11/23.
//

import SwiftUI


struct HotelListingView: View {
    
    @StateObject var viewModel:LisitingPageViewModel=LisitingPageViewModel()
    var body: some View {
        ScrollView {
            HotelFilterView(viewModel: viewModel)
            
            LazyVGrid(columns: [
                GridItem(.flexible(),spacing: 15),
                GridItem(.flexible())
            ],spacing: 15)
            {
                ForEach(viewModel.hotelList,id: \.self)
                {
                    hotel in
                    HotelListingItemView(hotel: hotel)
                }
            }
            .padding(.horizontal,10)
            
        }
        .task {
            await  viewModel.getHotelList()
            await  viewModel.getHotelFilters()
        }
    }
}

struct HotelFilterView: View {
    @ObservedObject var viewModel: LisitingPageViewModel
    
    var body: some View {
        VStack
        {
            HStack{
                HStack{
                    VStack(alignment: .leading)
                    {
                        Text("Select location")
                            .font(.robotoRegular(size: 14))
                        Text("\(viewModel.checkInDate.formatTo(format: "dd MMM")) - \(viewModel.checkOutDate.formatTo(format: "dd MMM")) | \(viewModel.roomCount) Rooms | \(viewModel.adultCount+viewModel.childCount) Guests")
                            .font(.robotoRegular(size: 12))
                            .foregroundColor(.appBlack77)
                    }
                    Spacer()
                    EditButton()
                    
                }
                .padding(.all,5)
                .rounderBorder(cornerRadius: 5)
                .onTapGesture {
                    viewModel.showExpandedFilter = true
                }
                SearchButton()
            }
            if(viewModel.showExpandedFilter){
                HStack(spacing: 20){
                    DatePickerTextField(date: $viewModel.checkInDate,header: "Check-In")
                    
                    DatePickerTextField(date: $viewModel.checkOutDate,header: "Check-Out")
                    
                }
                HStack{
                    AppStepper(header: "Rooms", count: $viewModel.roomCount, minCount: 1)
                    Spacer()
                    AppStepper(header: "Adult", count: $viewModel.adultCount, minCount: 1)
                    Spacer()
                    AppStepper(header: "Child", count: $viewModel.childCount, minCount: 0)
                }
                HStack{
                    YellowButton(text: "Search",width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                    Spacer()
                    GreyButton(text: "Cancel", width: UIScreen.main.bounds.width * 0.45)
                        .onTapGesture {
                            viewModel.showExpandedFilter = false
                        }
                }
            }
            
            else {
                HStack{
                    
                    DropDownMenuWithFixedTitile(optionList:viewModel.tripType , selecteOption: $viewModel.selectedTripType, label: {
                        option in
                        option.name ?? ""
                    }, header: "Trip Type",onTap:{
                        option in
                        print(option)
                        
                    })
                    Spacer()
                    
                    FilterByButton(bottomSheet: HotelFilterBottomSheet(hotelFilters: viewModel.hotelFilters),presentationDetent: [.large])
                    
                    Spacer()
                    DropDownMenuWithFixedTitile(optionList:viewModel.sortBy , selecteOption: $viewModel.selectedSortBY, label: {
                        option in
                        option.name ?? ""
                    }, header: "Sort By",onTap:{
                        option in
                        print(option)
                        
                    })
                }
            }
        }
        .padding(.horizontal,10)
    }
}

struct HotelFilterBottomSheet:View {
    let hotelFilters:[FilterItems]
    var body: some View {
        
        ScrollView{
            Spacer().frame(height: 30)
            ForEach(hotelFilters, id: \.self) { hotelFilter in
                
                VStack(alignment: .leading){
                    Text(hotelFilter.title ?? "")
                        .font(.robotoMedium(size: 16))
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    
                    FilterItemListing(valueList: hotelFilter.values ?? [])
                        .padding(.bottom,10)
                        .padding(.leading,15)
                    Divider()
                }
                
            }
            Spacer().frame(height: 30)
            YellowButton(text: "Apply")
        }
        
    }
}

struct FilterItemListing:View {
    let valueList:[SortbyItems]
    var body: some View {
        WrappingHStack(horizontalSpacing: 10,verticalSpacing: 10){
            ForEach(valueList,id: \.self)
            {
                value in
                Text(value.name ?? "")
                    .font(.robotoRegular(size: 14))
                    .padding(.vertical,5)
                    .padding(.horizontal,8)
                    .overlay(RoundedRectangle(cornerRadius: 8).stroke(Color.appGrey126))
                
            }
        }
        
    }
}

struct HotelListingItemView:View {
    let hotel :HotelForListing
    var body: some View {
        VStack(alignment: .leading){
            AppAsyncImage(url: hotel.imageUrl ?? "",width: UIScreen.main.bounds.width*0.43,height: 100,radius: 10)
                .padding(.bottom,5)
            HStack(alignment: .bottom){
                StarBuilder(numberOfStarts: (hotel.hotelStar ?? 0))
                Text(hotel.hotelType ?? "")
                    .font(.robotoMedium(size: 13))
                Spacer()
                if(Int(hotel.userRating ?? 0) != 0)
                {
                    HotelRatingView(rating: Int(hotel.userRating ?? 0),ratingCount: hotel.userRatingCount ?? 0)
                }
            }
            .padding(.bottom,5)
            
            Text(hotel.hotelName ?? "")
                .font(.robotoMedium(size: 14))
            Text(hotel.hotelAddress ?? "")
                .font(.robotoRegular(size: 12))
                .lineLimit(1)
            
            Text(hotel.shortDescription ?? "")
                .font(.robotoRegular(size: 10))
                .foregroundColor(.appBlack77)
                .lineLimit(2)
                .padding(.bottom,2)
            HStack(alignment: .bottom){
                PriceLabel(
                    amount: hotel.actualPrice ?? 0,
                    offerAmount:  hotel.offerPrice ?? 0,
                    fontSize: 13,
                    reduceFontSizeBy: 4,
                    newLine: true
                )
                Spacer()
                TaxAndFee(amount: Int(hotel.serviceChargeValue ?? 0), fontSize: 10)
            }
        }
        .padding(.horizontal,5)
        .padding(.top,5)
        .padding(.bottom,8)
        .frame(height:240)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 13))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)
        
    }
}

struct StarBuilder:View {
    let numberOfStarts:Int
    var body: some View {
        HStack(alignment: .bottom, spacing:2){
            ForEach((0...(numberOfStarts)),id: \.self)
            {
                start in
                Image(systemName: "star.fill")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 5)
            }
        }
    }
}

struct HotelRatingView:View {
    let rating:Int
    let ratingCount:Int
    var body: some View {
        HStack(spacing:5){
            Text("\(rating < 1 ? "Average" : rating < 3 ? "Good" : "Very Good") \n(\(ratingCount) rating)" )
                .font(.robotoRegular(size: 8))
                .multilineTextAlignment(.trailing)
            Text("\(String(rating))/5")
                .font(.robotoRegular(size: 11))
                .foregroundColor(.appWhite)
                .padding(.horizontal,5)
                .padding(.vertical,2)
                .background(Color.appBlue.gradient)
                .cornerRadius(5)
            
        }
        
    }
}


#Preview{HotelListingView()}



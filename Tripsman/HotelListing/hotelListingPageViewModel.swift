//
//  listingPageController.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 10/11/23.
//

import Foundation
class LisitingPageViewModel:ObservableObject
{
    
    
    @Published var checkInDate=Date()
    @Published var checkOutDate=Date().addingTimeInterval(60*60*24*2)
    @Published var  roomCount=1
    @Published var guestCount=1
    
    
    @Published var hotelList:[HotelForListing]=[]
    
    
    
    @Published var hotelFilters:[FilterItems]=[]
    @Published var sortBy:[SortbyItems]=[]
    @Published var tripType:[SortbyItems]=[]
    @Published var selectedHotelFilters:[String :Any]=[:]
    @Published var selectedSortBY:SortbyItems?
    @Published var selectedTripType:SortbyItems?
    @Published var isHotleFilterBottomSheetShowing:Bool = false
    @Published var showExpandedFilter:Bool = false
    
    
    @Published var adultCount:Int = 1
    @Published var childCount:Int = 0
    
    func getHotelList()async{
        do
        {
            var body:[String:Any]=["HotelRateTo":100000,
                                   "offset":0,
                                   "ChildCount":0,
                                   "CheckInDate":"2023/11/11",
                                   "HotelRateFrom":0,
                                   "RoomCount":1,
                                   "Language":"en",
                                   "Currency":"INR",
                                   "SortBy":"",
                                   "HotelFilters":[:],
                                   "Country":"IND",
                                   "CheckOutDate":"2023/11/12",
                                   "recordCount":20,
                                   "AdultCount":2
            ]
            let response:HotelListingModel=try await ApiServices.getHotelListing(body: body)
            hotelList=response.data ?? []
        }
        catch
        {
            print(error)
        }
    }
    
    func getHotelFilters()async
    {
        do
        {
            let response:FilterResponseModel = try await ApiServices.getHotelFilter(queryItems: [UserDefaultKeys.language:UserDefaults.standard.string(forKey:UserDefaultKeys.language) ?? ""])
            hotelFilters=response.data?.filtes ?? []
            tripType=response.data?.tripTypes ?? []
            sortBy=response.data?.sortby ?? []
        }
        catch
        {
            print(error)
        }
    }
}

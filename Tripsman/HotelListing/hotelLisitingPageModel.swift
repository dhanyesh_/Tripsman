//
//  lisitingPageModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 10/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let hotelListingModel = try? JSONDecoder().decode(HotelListingModel.self, from: jsonData)

// MARK: - HotelListingModel
struct HotelListingModel: Hashable, Codable {
    let totalRecords: Int?
    let data: [HotelForListing]?
    let status: Int?
    let message: String?
}

// MARK: - HotelForListing
struct HotelForListing: Hashable, Codable {
    let hotelId: Int?
    let hotelName: String?
    let hotelAddress: String?
    let hotelCity: String?
    let hotelState: String?
    let hotelLatitude: String?
    let hotelLongitude: String?
    let hotelEmail: String?
    let hotelMobile: String?
    let shortDescription: String?
    let description: String?
    let propertyRules: String?
    let termsAndCondition: String?
    let serviceChargeType: String?
    let actualPrice: Int?
    let offerPrice: Int?
    let serviceChargeValue: Double?
    let hotelStatus: Int?
    let imageUrl: String?
    let userRating: Int?
    let userRatingCount: Int?
    let isSponsored: Int?
    let hotelStar: Int?
    let hotelType: String?
    let currency: String?
    let hotelAmenities: [HotelAmenity]?
}

// MARK: - HotelAmenity
struct HotelAmenity: Hashable, Codable {
    let facilityName: String?
    let facilityICon: String?
    let facilityStatus: Int?
    let facilityId: Int?
    let hotelId: Int?
    let id: Int?
}

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let meetUpListingModel = try? JSONDecoder().decode(MeetUpListingModel.self, from: jsonData)


import Foundation

// MARK: - FilterResponseModel
struct FilterResponseModel: Hashable, Codable {
    let totalRecords: Int?
    let data: FilterTypes?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct FilterTypes: Hashable, Codable {
    let filtes: [FilterItems]?
    let filters: [FilterItems]?
    let sortby: [SortbyItems]?
    let tripTypes: [SortbyItems]?
}

// MARK: - Filte
struct FilterItems: Hashable, Codable {
    let title: String?
    let type: String?
    let filterKey: String?
    let values: [SortbyItems]?
}

// MARK: - Sortby
struct SortbyItems: Hashable, Codable {
    let name: String?
    let id: Int?
}

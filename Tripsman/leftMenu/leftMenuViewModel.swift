//
//  leftMenuViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 17/11/23.
//

import Foundation
class LeftMenuViewModel:ObservableObject
{
    
    @Published var isLoginSessionShowing:Bool=false
    
    let loginOrSignUp:LeftMenuItemModel=LeftMenuItemModel(header: "Login / Sign Up Now", subHeader: "Login for best deals", icon:"person.circle", blueText: true )
    let userDeatils:LeftMenuItemModel=LeftMenuItemModel(header:"Hai, \(UserDefaults.standard.string(forKey: UserDefaultKeys.userName) ?? "User")",
                                                          subHeader: UserDefaults.standard.string(forKey: UserDefaultKeys.userEmail) ?? "email unavalible",
                                                          icon:"person.circle")
   
    let myTrips:LeftMenuItemModel=LeftMenuItemModel(header: "My Trips", subHeader: "View your trips", icon:"briefcase")
    let logOut:LeftMenuItemModel=LeftMenuItemModel(header: "Logout", subHeader: "", icon:"return")
   
}

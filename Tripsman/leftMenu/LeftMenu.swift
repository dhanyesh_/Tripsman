//
//  LeftMenu.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 17/11/23.
//

import SwiftUI



struct LeftMenu: View {
    @Binding var isShowing: Bool
    @StateObject var viewModel:LeftMenuViewModel=LeftMenuViewModel()
  
    var edgeTransition: AnyTransition = .move(edge: .leading)
    var body: some View {
        VStack
        { 
            if(isShowing){
                VStack(alignment: .leading,spacing: 8){
                    Spacer()
                        .frame(height:100)
                    if((UserDefaults.standard.string(forKey: UserDefaultKeys.token) ?? "").isEmpty)
                    {
                        LeftMenuItem(leftMenuItem: viewModel.loginOrSignUp)
                            .onTapGesture {
                                viewModel.isLoginSessionShowing=true
                            }
                            .sheet(isPresented: $viewModel.isLoginSessionShowing)
                        {
                            LoginView()
                                .presentationDetents([.large])
                        }
                    }
                else
                    {
                        LeftMenuItem(leftMenuItem: viewModel.userDeatils)
                    }
                    
                    Divider()
                    LeftMenuItem(leftMenuItem: viewModel.myTrips)
                    Divider()
                    LeftMenuItem(leftMenuItem: viewModel.logOut)
                    Divider()
                    Spacer()
                }
               
                .frame(width: UIScreen.main.bounds.width*0.7,height: UIScreen.main.bounds.height)
                .background(Color.appWhite)
                .shadow(color: .purple.opacity(0.1), radius: 10, x: 0, y: 3)
                .animation(/*@START_MENU_TOKEN@*/.easeIn/*@END_MENU_TOKEN@*/)
                .transition(edgeTransition)
                .gesture(DragGesture()
                    .onEnded({ value in
                        if value.translation.width < 0 {
                            isShowing=false
                        }
                    }
                ))
            }
        }
    }
}



struct LeftMenuItem:View {
    let leftMenuItem:LeftMenuItemModel
    var body: some View {
        HStack(alignment: .top){
            Image(systemName: leftMenuItem.icon)
                .foregroundColor(.appGrey126)
            VStack(alignment: .leading){
                Text(leftMenuItem.header)
                     .font(.robotoRegular(size: 16))
                     .foregroundColor((leftMenuItem.blueText ?? false) ? .appBlue : .appBlack34)
                if(!leftMenuItem.subHeader.isEmpty){
                    Text(leftMenuItem.subHeader)
                         .font(.robotoRegular(size: 13))
                         .foregroundColor(.appGrey126)
                }
            }
        }
        .padding(.horizontal,10)
      
    }
}


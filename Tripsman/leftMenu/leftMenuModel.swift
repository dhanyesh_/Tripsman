//
//  leftMenuModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 17/11/23.
//

import Foundation
struct LeftMenuItemModel
{
    let header:String
    let subHeader:String
    let icon:String
    let blueText:Bool?
    init(header: String, subHeader: String, icon: String, blueText: Bool?=nil) {
        self.header = header
        self.subHeader = subHeader
        self.icon = icon
        self.blueText = blueText
    }
}

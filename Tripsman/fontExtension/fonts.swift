//
//  fonts.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 02/11/23.
//
import Foundation
import SwiftUI



extension Font{
    
    static func robotoLight(size: CGFloat = 18) -> Font {
          return .custom("Roboto-Light", size: size)
      }
    static func robotoThin(size: CGFloat = 18) -> Font {
          return .custom("Roboto-Thin", size: size)
      }
    static func robotoRegular(size: CGFloat = 18) -> Font {
          return .custom("Roboto-Regular", size: size)
      }
    static func robotoMedium(size: CGFloat = 18) -> Font {
          return .custom("Roboto-Medium", size: size)
      }
    static func robotoBold(size: CGFloat = 18) -> Font {
          return .custom("Roboto-Bold", size: size)
      }
 }
      
     


//
//  loginView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import SwiftUI

struct LoginView: View {
    @StateObject var viewModel=LoginViewModel()
    var body: some View {
        
        NavigationStack{
            ZStack(alignment: .top)
            {
                Image(AppImages.loginBackGround)
                    .resizable()
                    .scaledToFill()
                    .ignoresSafeArea()
                VStack {
                    AppLogoWithText()
                        .padding(.vertical,30)
                    
                  
                    VStack{
                    
                        Text("LOGIN")
                            .font(.robotoMedium(size:23))
                        
                        AppTextField(text: $viewModel.userName, placeHolder: "Email/ Mobile Number",width: UIScreen.main.bounds.width-60)
                        
                        PasswordTextField(text: $viewModel.password, isSecured: viewModel.showPassword, width: UIScreen.main.bounds.width-60, placeHolder: "Password" )
                            .underlineBorder()
                            .padding(.bottom,20)
                        
                        BlueButton(text:"Login", width:UIScreen.main.bounds.width-65)
                            .onTapGesture {
                              
                            Task
                                {
                                    await viewModel.login()
                                }
                               
                            }
                        
                        GreyButton(text:"Forgot Password ?", width:UIScreen.main.bounds.width-65)
                        
                        Text("or")
                            .font(.robotoMedium(size: 15))
                            .foregroundColor(.appWhite)
                        HStack{
                            RegistrationMethodView(registationMethod: viewModel.loginWithGoogle)
                            Spacer()
                            RegistrationMethodView(registationMethod: viewModel.loginWIthApple)
                            Spacer()
                           NavigationLink
                            {
                                RegistrationView()
                            }
                        label:{
                               RegistrationMethodView(registationMethod: viewModel.createAnAccount)
                           }
                        }
                        
                     
                        
                    }
                    .padding(20)
                    .frame(width: UIScreen.main.bounds.width-30)
                    .background(Color.appYellow.opacity(0.75))
                .cornerRadius(10)
                }
              
            }
        }
        
       
      
    }}

struct RegistrationMethodView: View {
let registationMethod:RegistrationMethodModel
    var body: some View {
        VStack(spacing: 5)
        {
            Image(registationMethod.icon)
                .resizable()
                .frame(width: 22,height: 22)
            Text(registationMethod.name)
                .multilineTextAlignment(.center)
                .foregroundColor(.appWhite)
                .font(.robotoMedium(size: 12))
            
        }
        .padding(.horizontal,18)
        .padding(.vertical,8)
        .overlay(RoundedRectangle(cornerRadius: 10)
            .stroke(Color.appWhite,lineWidth: 1))
    }
}



struct Previews_LoginView_Previews: PreviewProvider {
    static var previews: some View {
      LoginView()
    }
}




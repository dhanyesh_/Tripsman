//
//  loginModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation

struct RegistrationMethodModel
{
    let name:String
    let icon:String
    init(name: String, icon: String) {
        self.name = name
        self.icon = icon
    }
}

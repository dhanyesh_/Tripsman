//
//  Icons.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation
class AppIcons
{
    static let bed:String = "bed"
    static let menu:String = "menu"
    static let logo:String = "logo"
    static let india:String = "india"
    static let wallet:String = "wallet"
    static let perosn:String = "person"
    static let people:String = "people"
    static let language:String = "language"
    static let arrowLeft:String = "arrowLeft"
    static let arrowDown:String = "arrowDown"
    static let navBarHome:String = "navBarHome"
    static let passwordShow:String = "passwordShow"
    static let passwordHide:String = "passwordHide"
    static let navBarWallet:String = "navBarWallet"
    static let tripsmanText:String = "tripsmanText"
    static let notification:String = "notification"
    static let navBarProfile:String = "navBarProfile"
    static let navBarMyTrips:String = "navBarMyTrips"
    static let loginWithApple:String = "loginWithApple"
    static let loginWithGoogle:String = "loginWithGoogle"
    static let loginWithAccount:String = "loginWithAccount"
    
}

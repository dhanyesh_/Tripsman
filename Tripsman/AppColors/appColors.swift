//
//  appColors.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation
import SwiftUI
extension Color
{
    static let appBlack34 = Color("appBlack34")
    static let appBlack77 = Color("appBlack77")
    static let appBlue = Color("appBlue")
    static let appGreen = Color("appGreen")
    static let appGrey126 = Color("appGrey126")
    static let appGrey227 = Color("appGrey227")
    static let appGrey239 = Color("appGrey239")
    static let appWhite = Color("appWhite")
    static let appRed = Color("appRed")
    static let appYellow = Color("appYellow")
}

//
//  walletModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let walletResponseModel = try? JSONDecoder().decode(WalletResponseModel.self, from: jsonData)


// MARK: - WalletResponseModel
struct WalletResponseModel: Hashable, Codable {
    let totalRecords: Int?
    let data: WalletItems?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct WalletItems: Hashable, Codable {
    let totalPoints: Int?
    let usedPoints: Int?
    let addedPoints: Int?
    let detail: [WalletItemsDetail]?
}

// MARK: - Detail
struct WalletItemsDetail: Hashable, Codable {
    let bookingId: Int?
    let moduleCode: String?
    let points: Int?
    let date: String?
    let description: String?
    let currencyValue: String?
}

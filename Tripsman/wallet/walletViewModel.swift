//
//  walletViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import Foundation
class WalletViewModel:ObservableObject
{
    @Published var totalPoints:Int=0
    @Published var wallerData:[WalletItemsDetail]=[]
    @Published var pageCount=0
    @Published var total=0
    func getWalletDetails()async
    {
     do
     {
//         var response : WalletResponseModel = try await  ApiServices.getCustomerWalletDetails(queryItems: [
//            UserDefaultKeys.language:UserDefaults.standard.string(forKey:UserDefaultKeys.language) ?? "",
//            UserDefaultKeys.country:UserDefaults.standard.string(forKey:UserDefaultKeys.country) ?? ""]
         
         let response = try JSONDecoder().decode(WalletResponseModel.self, from: Data(
         """
         {"totalRecords":228,"data":{"totalPoints":2662,"usedPoints":20340,"addedPoints":23002,"detail":[{"bookingId":768,"moduleCode":"HDY","points":950.00,"date":"2023-11-14T05:48:42","description":"HLD-768-Credit - Booking","currencyValue":"0"},{"bookingId":1818,"moduleCode":"HTL","points":40.00,"date":"2023-11-13T07:38:06","description":"HTL-1818-Credit - Booking","currencyValue":"0"},{"bookingId":784,"moduleCode":"ACT","points":0.00,"date":"2023-11-10T04:01:10","description":"ACT-003783-Credit - Booking","currencyValue":"0"},{"bookingId":1808,"moduleCode":"HTL","points":50.00,"date":"2023-11-10T03:26:57","description":"HTL-1808-Credit - Booking","currencyValue":"0"},{"bookingId":742,"moduleCode":"HDY","points":40.00,"date":"2023-11-09T12:12:03","description":"HLD-742-Credit - Booking","currencyValue":"0"},{"bookingId":1807,"moduleCode":"HTL","points":6.00,"date":"2023-11-09T12:08:38","description":"HTL-1807-Credit - Booking","currencyValue":"0"},{"bookingId":1804,"moduleCode":"HTL","points":20.00,"date":"2023-11-09T09:23:28","description":"HTL-1804-Credit - Booking","currencyValue":"0"},{"bookingId":1767,"moduleCode":"HTL","points":14.00,"date":"2023-11-06T09:56:37","description":"HTL-1767-Credit - Booking","currencyValue":"0"},{"bookingId":1767,"moduleCode":"HTL","points":-300.00,"date":"2023-11-06T09:56:06","description":"HTL-1767- Debit - Booking","currencyValue":"0"},{"bookingId":730,"moduleCode":"HDY","points":142.00,"date":"2023-11-06T09:54:27","description":"HLD-730-Credit - Booking","currencyValue":"0"},{"bookingId":730,"moduleCode":"HDY","points":-300.00,"date":"2023-11-06T09:53:56","description":"HLD-730- Debit - Booking","currencyValue":"0"},{"bookingId":769,"moduleCode":"ACT","points":388.00,"date":"2023-11-06T09:51:28","description":"ACT-003768-Credit - Booking","currencyValue":"0"},{"bookingId":769,"moduleCode":"ACT","points":-300.00,"date":"2023-11-06T01:48:39","description":"ACT-003768- Debit - Booking","currencyValue":"0"},{"bookingId":768,"moduleCode":"ACT","points":118.00,"date":"2023-11-03T04:33:18","description":"ACT-003767-Credit - Booking","currencyValue":"0"},{"bookingId":765,"moduleCode":"ACT","points":234.00,"date":"2023-11-03T04:26:13","description":"ACT-003764-Credit - Booking","currencyValue":"0"},{"bookingId":765,"moduleCode":"ACT","points":-300.00,"date":"2023-11-02T21:25:35","description":"ACT-003764- Debit - Booking","currencyValue":"0"},{"bookingId":727,"moduleCode":"HDY","points":1799.00,"date":"2023-11-03T04:22:57","description":"HLD-727-Credit - Booking","currencyValue":"0"},{"bookingId":727,"moduleCode":"HDY","points":-61.00,"date":"2023-11-03T04:21:56","description":"HLD-727- Debit - Booking","currencyValue":"0"},{"bookingId":1764,"moduleCode":"HTL","points":18.00,"date":"2023-11-03T04:15:39","description":"HTL-1764-Credit - Booking","currencyValue":"0"},{"bookingId":1764,"moduleCode":"HTL","points":-104.00,"date":"2023-11-03T04:14:32","description":"HTL-1764- Debit - Booking","currencyValue":"0"}]},"status":1,"message":"Success"}
         """.utf8
))
         totalPoints = response.data?.totalPoints ?? 0
         wallerData = response.data?.detail ?? []
         total = response.data?.totalPoints ?? 0
     }
        catch
        {
            print(error)
        }
    }
}

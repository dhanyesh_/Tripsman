//
//  walletView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.


import SwiftUI

struct WalletView: View {
    @StateObject var viewModel:WalletViewModel=WalletViewModel()
    var body: some View {
        ScrollView{
            WallerMyPoints(totalPoints: viewModel.totalPoints)
            Spacer()
                .frame(height: 40)
            HStack(alignment: .top){
                Text("Date")
                   
                    .frame(width: UIScreen.main.bounds.width*0.25,alignment: .leading)
                    .foregroundColor(Color.appBlack77)
                    .font(.robotoMedium(size: 16))
                    .padding(.trailing,10)
                Text("Description" )
                    .frame(width: UIScreen.main.bounds.width*0.4,alignment: .leading)
                    .foregroundColor(Color.appBlack77)
                    .font(.robotoMedium(size: 16))
                    .padding(.trailing,10)
                Text("Points")
                    .frame(width: UIScreen.main.bounds.width*0.2,alignment: .leading)
                    .foregroundColor(Color.appBlack77)
                    .font(.robotoMedium(size: 16))
            }
            Divider()
            WalletTableData(wallerData: viewModel.wallerData)
            
        }
            .task
        {
            await viewModel.getWalletDetails()
        }
    }
        
}


struct WalletTableData:View {
    let wallerData:[WalletItemsDetail]
    var body: some View {
        ForEach(wallerData,id: \.self)
        {
            walletItem in
            HStack(alignment: .top){
                Text(walletItem.date?.convertISODateTo(fromat: "dd MMM yyyy") ?? "" )

                    .frame(width: UIScreen.main.bounds.width*0.25,alignment: .leading)
                    .font(.robotoMedium(size: 16))
                    .padding(.trailing,10)
                Text(walletItem.description ?? "" )
                    .frame(width: UIScreen.main.bounds.width*0.4,alignment: .leading)
                    .font(.robotoMedium(size: 16))
                Text(String(walletItem.points ?? 0))
                    .padding(.trailing,10)
                    .frame(width: UIScreen.main.bounds.width*0.2,alignment: .leading)
                    .font(.robotoMedium(size: 16))
                    .foregroundColor(
                        walletItem.points ?? 0 < 0 ? .appRed
                    :   walletItem.points ?? 0 > 0 ? .appGreen
                    :   .appBlack34)
            }
            .padding(.vertical,8)
            Divider()
            
        }
    }
}


struct WallerMyPoints:View {
    let totalPoints:Int
    var body: some View {
        HStack{
            Image(AppIcons.wallet)
                .resizable()
                .scaledToFit()
                .frame(width: 50)
            Divider()
            VStack(alignment: .leading)
            {
                Text("Total Points")
                    .font(.robotoMedium(size: 16))
                Text("\(totalPoints)")
                    .font(.robotoRegular(size: 25))
            }
            Spacer()
        }
        
        .padding()
        .background(Color.appWhite)
        
        .frame(width: UIScreen.main.bounds.width-30)
        .cornerRadius(10)
        .shadow(color: .appBlack34.opacity(0.2), radius: 10)
        .padding(.horizontal,15)
    }
}

#Preview {
    WalletView()
}

//
//  ApiErrors.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation

enum ApiError :Error
{
case invalidURL
case unAuthorized
case inVallidResponse
case jsonEncodingError(Error)
case customError(message:String)
}

enum AppError: Error
{
   
}

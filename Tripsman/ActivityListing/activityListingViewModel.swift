//
//  activityListingViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import Foundation
class ActivityListingViewModel:ObservableObject
{
    @Published var activityList:[ActivityForListing]=[]
    @Published var showExpandedFilter:Bool=false
    @Published var country:String="All"
    @Published var date:Date=Date()
    @Published var filters:[FilterItems]=[]
    @Published var sortBy:[SortbyItems]=[]
    @Published var selectedSortBy:SortbyItems?
    
    func getActivityList()async{
        do
        {
            var body:[String:Any]=["sortBy": "", "recordCount": 20, "Country": "IND", "budgetFrom": 0, "activityCountry": "", "Currency": "INR", "Language": "en", "offset": 0, "budgetTo": 100000, "activityFilters": [:]]
            var response:ActivityListingModel=try await ApiServices.getActivityListingListing(body: body)
            activityList=response.data ?? []
        }
        catch
        {
            print(error)
        }
    }
    
    func getActivityFilters()async
    {
        do
        {
            let response:FilterResponseModel = try await ApiServices.getActivityFilter(queryItems: [UserDefaultKeys.language:UserDefaults.standard.string(forKey:UserDefaultKeys.language) ?? ""])
            
            filters=response.data?.filters ?? []
            sortBy=response.data?.sortby ?? []
        }
        catch
        {
            print(error)
        }
    }
}

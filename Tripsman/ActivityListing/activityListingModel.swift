//
//  activityListingModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let activityListingModel = try? JSONDecoder().decode(ActivityListingModel.self, from: jsonData)

import Foundation

// MARK: - ActivityListingModel
struct ActivityListingModel: Hashable, Codable {
    let totalRecords: Int?
    let data: [ActivityForListing]?
    let status: Int?
    let message: String?
}

// MARK: - Datum
struct ActivityForListing: Hashable, Codable {
    let activityId: Int?
    let activityName: String?
    let activityCode: String?
    let countryId: Int?
    let contactPerson: String?
    let contactName: String?
    let shortDescription: String?
    let contactNumber: String?
    let contactEmail: String?
    let overview: String?
    let features: String?
    let termsAndConditions: String?
    let activityLocation: String?
    let highlights: String?
    let activityDuration: String?
    let costPerPerson: Int?
    let offerPrice: Int?
    let serviceChargeValue: Int?
    let activityAmount: String?
    let activityStatus: String?
    let isSponsored: Int?
    let latitude: Double?
    let longitude: Double?
    let activityImages: [ActivityImage]?
    let activityType: [ActivityType]?
    let activityInclusion: [ActivityInclusion]?
}

// MARK: - ActivityImage
struct ActivityImage: Hashable, Codable {
    let id: Int?
    let imageUrl: String?
    let isFeatured: Int?
    let activityId: Int?
    let status: Int?
}

// MARK: - ActivityInclusion
struct ActivityInclusion: Hashable, Codable {
    let inclusionName: String?
    let aRInclusionName: String?
    let inclusionICon: String?
    let inclusionStatus: Int?
    let inclusionId: Int?
    let activityId: Int?
    let status: Int?
}

// MARK: - ActivityType
struct ActivityType: Hashable, Codable {
    let activityTypeName: String?
    let aRActivityTypeName: String?
    let activityTypeId: Int?
    let activityId: Int?
    let id: Int?
    let status: Int?
}

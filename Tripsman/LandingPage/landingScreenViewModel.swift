//
//  landingScreenViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import Foundation
class LandingPageViewModel:ObservableObject
{
    
    let bottomNavigationBarItems:[String] =
    [
        AppIcons.navBarHome,
        AppIcons.navBarMyTrips,
        AppIcons.navBarWallet,
        AppIcons.navBarProfile
    ]
    
    @Published var showLeftMenu:Bool=false
    @Published var selectedIndex:Int=0
    
}

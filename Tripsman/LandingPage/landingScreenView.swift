//
//  landingScreenView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 01/11/23.
//

import SwiftUI

struct LandingScreenView: View {
    @StateObject var viewModel=LandingPageViewModel()
  
    var body: some View {
        NavigationStack{
            ZStack(alignment: .leading){
               
                VStack{
                    HomePageAppBar(isShowing: $viewModel.showLeftMenu)
                    TabView(selection: $viewModel.selectedIndex)
                    {
                        HomeScreenView()
                            .tag(0)
                        MyTripsView()
                            .tag(1)
                        WalletView()
                            .tag(2)
                        profileView()
                            .tag(3)
                      .onAppear()
                        {
                            UITabBar.appearance().backgroundColor = UIColor(.appYellow.opacity(1))
                            
                        }
                        
                    }
                    BottonNavigatinBar(bottomNavigationBarItem: viewModel.bottomNavigationBarItems,selectedIndex:$viewModel.selectedIndex)
                    
                }
                .tabViewStyle(.page(indexDisplayMode: .never))
                .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .never))
             
            
                LeftMenu(isShowing: $viewModel.showLeftMenu)
              
                
            }
            .edgesIgnoringSafeArea(.bottom)
        }
        
        
    }
}


struct BottonNavigatinBar:View {
    let bottomNavigationBarItem:[String]
    @Binding var selectedIndex:Int
    var body: some View {
        HStack()
        {
            ForEach(bottomNavigationBarItem,id: \.self)
            {
                item in
                Image(item)
                    .resizable()
                    .renderingMode(.template)
                    .foregroundColor(
                        bottomNavigationBarItem.firstIndex(of: item) ?? 0 == selectedIndex ?
                        Color.appWhite:Color.appBlack34
                    )
                    .scaledToFit()
                    .frame(width: 35)
                    .onTapGesture {
                        selectedIndex = bottomNavigationBarItem.firstIndex(of: item) ?? 0
                    }
            
            }
            .frame(maxWidth: .infinity)
          
        
        }
        
        .padding(.bottom,20)
        .padding(.top,10)
        .background(Color.appYellow.gradient)
       
    }
    
}

struct HomePageAppBar: View {
    @Binding var isShowing: Bool
    var body: some View {
        HStack
        {
            Image(AppIcons.menu)
                .resizable()
                .scaledToFit()
                .frame(width:30)
                .padding(.trailing,10)
                .onTapGesture {
                isShowing.toggle()
                }
            
            Image(AppIcons.logo)
                .resizable()
                .scaledToFit()
                .frame(width:30)
              
            Spacer()
            Image(AppIcons.india)
                .resizable()
                .scaledToFit()
                .frame(width:30)
             
            Image(AppIcons.language)
                .resizable()
                .scaledToFit()
                .frame(width:30)
               
            Image(AppIcons.notification)
                .resizable()
                .scaledToFit()
                .frame(width:30)
               
            
        }
        .padding(.horizontal,15)
    }
}
struct landingScreenView_Previews: PreviewProvider {
    static var previews: some View {
        LandingScreenView()
    }
}

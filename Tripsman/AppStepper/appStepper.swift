//
//  appStepper.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 15/11/23.
//

import SwiftUI

struct AppStepper: View {
    let header:String
    @Binding var count:Int
    let minCount:Int
   
    var body: some View {
        HStack{
            Image(systemName: "minus")
                .resizable()
                .scaledToFit()
                .frame(width: 11)
                .padding(.vertical,8)
                .padding(.leading,5)
                .onTapGesture {
                    if(count != minCount)
                    {
                        count = count-1
                    }
                }
            Text("\(header) \(count)")
                .font(.robotoRegular(size: 11))
                .padding(.vertical, 8)
                .padding(.horizontal,5)
                .rounderBorder(cornerRadius: 0)
            Image(systemName: "plus")
                .resizable()
                .scaledToFit()
                .frame(height: 11)
                .padding(.vertical,8)
                .padding(.trailing,5)
                .onTapGesture {
                   
                        count = count+1
                    
                }
        }
        .rounderBorder(cornerRadius: 5)
    }
}


//
//  MyTripsModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let myTripsResponse = try? JSONDecoder().decode(MyTripsResponse.self, from: jsonData)

import Foundation

// MARK: - MyTripsResponse
struct MyTripsResponse:Codable, Hashable {
    let totalRecords: Int?
    let data: [MyTrip]?
    let status: Int?
    let message: String?
}

// MARK: - MyTrip
struct MyTrip: Codable, Hashable {
    let module: String?
    let bookingID: Int?
    let bookingNo: String?
    let bookedDate: String?
    let totalAmount: Double?
    let checkInDate: String?
    let checkOutDate: String?
    let checkInTime: String?
    let checkOutTime: String?
    let customerCode: String?
    let customerID: String?
    let customerName: String?
    let totalGuest: Int?
    let primaryGuest: String?
    let contactNo: String?
    let gender: String?
    let age: Int?
    let id: Int?
    let isCancelled: Int?
    let tripStatusValue: Int?
    let tripStatus: String?
    let name: String?
    let imageURL: String?
    let adultCount: Int?
    let childCount: Int?
    let tripMessage: String?
    let bookingMessage: String?
    let bookingStatus: String?
    let roomCount: Int?
    let place: String?
    let location: String?
    let vendor: String?
    
}


struct SortAndFilterForMyTrips:Hashable{
    let text:String
    let code:String
}

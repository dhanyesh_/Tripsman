//
//  MyTripsViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import Foundation
class MyTripsViewModel: ObservableObject
{
    
    
    @Published  var selectedBookingStatus = ""
    let bookingStatus:[String]=["All","UpComing","Completed","Cancelled"]
    let sortBy:[SortAndFilterForMyTrips]=[
        SortAndFilterForMyTrips(text: "Latest first", code: "DESCDATE"),
        SortAndFilterForMyTrips(text: "Oldest first", code: "ASCDATE"),
    ]  
    let filterBy:[SortAndFilterForMyTrips]=[
        SortAndFilterForMyTrips(text: "Hotel", code: "HTL"),
        SortAndFilterForMyTrips(text: "Holiday Package", code: "HDY"),
        SortAndFilterForMyTrips(text: "Activities", code: "ACT"),
        SortAndFilterForMyTrips(text: "MeetUps", code: "MTP"),
    ]
    
    @Published var selectedFilterBy:SortAndFilterForMyTrips?
    @Published var selecteSortBy:SortAndFilterForMyTrips?
    
    var body:[String:String]=[:

    ]
    let sortByOptions:[String]=["Latest first","Oldest first"]
    let filterByOptions:[String]=["Hotels","Holiday Packages","Activites","Meetups"]
    @Published var myTrips:[MyTrip]=[]
    @Published var controllerSearchField=""
    func getMyTrips()async
    {
      do{
        
          body=["language":UserDefaults.standard.string(forKey: UserDefaultKeys.language) ?? "en",
                "module_code":selectedFilterBy?.code ?? "",
                 "search_text":controllerSearchField,
                 "booking_status":selectedBookingStatus,
                "sortby":selecteSortBy?.code ?? "",
                 "offset":"0",
                 "recordCount":"20"]
          let response:MyTripsResponse = try await ApiServices.getMyTrips(withBody: body)
          self.myTrips=response.data ?? []
       }
        catch
        {
            print(error)
        }
    }
}




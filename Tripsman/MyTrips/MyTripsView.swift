//
//  MyTripsView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 03/11/23.
//

import SwiftUI

struct MyTripsView: View {
    @StateObject var viewModel:MyTripsViewModel = MyTripsViewModel()
   
    var body: some View {
        NavigationStack{
            VStack
            {
                MyTipsBookingTypeSelector(selectedBookingStatus: $viewModel.selectedBookingStatus, bookingStatus: viewModel.bookingStatus,onTap:
                                            {
                    Task{
                       await viewModel.getMyTrips()
                    }
                })
                
                SearchAppTextField(text: $viewModel.controllerSearchField, placeHolder: "Search For a booking")
            
                HStack {
                    DropDownMenuWithFixedTitile(optionList: viewModel.filterBy, selecteOption: $viewModel.selectedFilterBy, label:
                                    {
                        option in
                        return option.text
                    }, header: "Filter By",onTap:{
                        option in
                        viewModel.selectedFilterBy=option
                        
                        Task{
                            await viewModel.getMyTrips()
                        }
                        
                    }
                    )
                    
                    Spacer()
                        .frame(width: 25)
                    
                    DropDownMenuWithFixedTitile(optionList: viewModel.sortBy, selecteOption: $viewModel.selecteSortBy, label:
                                    {
                        option in
                        return option.text
                    },header: "Sort By",onTap:{
                        option in
                        Task{
                           await viewModel.getMyTrips()
                        }
                        
                    })
                }
                   Spacer()
                    .frame(height: 8)
                ScrollView
                {
                    Spacer()
                     .frame(height: 8)
                    ForEach(viewModel.myTrips,id: \.self)
                    {
                        
                        trip in
                        
                        if(trip.module == "ACT")
                        
                        {
                            NavigationLink(destination:MyTripsDetailsView(module: .activity, id: trip.id ?? 0),
                                           label:
                            {
                                MyTripsActivityContainer(trip:trip)
                                 
                            })
                            .buttonStyle(PlainButtonStyle())
                        }
                        
                        if(trip.module == "HTL")
                            
                            {
                                NavigationLink(destination:MyTripsDetailsView(module: .hotel, id: trip.id ?? 0),
                                               label:
                                {
                                    MyTripsHotelContainer(trip:trip)
                                     
                                })
                                .buttonStyle(PlainButtonStyle())
                            }
                        
                     
                        if(trip.module == "HDY")
                            
                            {
                                NavigationLink(destination:MyTripsDetailsView(module: .holidayPackage, id: trip.id ?? 0),
                                               label:
                                {
                                    MyTripsHolidayPackageContainer(trip:trip)
                                     
                                })
                                .buttonStyle(PlainButtonStyle())
                            }
                          
                     
                        if(trip.module == "MTP")
                            
                            {
                                NavigationLink(destination:MyTripsDetailsView(module: .meetUps, id: trip.id ?? 0),
                                               label:
                                {
                                    MyTripsMeetUpContainer(trip:trip)
                                     
                                })
                                .buttonStyle(PlainButtonStyle())
                            }
                       
                
                    }
                }
            }
            .padding(.horizontal,10)
            .padding(.top,10)
                .task {
                   await viewModel.getMyTrips()
            }
        }
           
    }
}


struct IconWithTextMyTrips:View {
    let image:String
    let text:String
    var body: some View {
        HStack(alignment: .bottom)
        {
            Image(image)
                .resizable()
                .scaledToFit()
                .frame(height: 13)
            Text(text)
                .font(.robotoRegular(size: 10))
                .foregroundColor(.appGrey126)
        }
    }
}


struct MyTripsView_Previews: PreviewProvider {
    static var previews: some View {
        MyTripsView()
    }
}

struct MyTripsActivityContainer: View {
    let trip:MyTrip
    var body: some View {
        VStack{
            HStack(spacing:10) {
                AppAsyncImage(url: trip.imageURL ?? "",width: 95,height:80,radius: 10)
                VStack(alignment: .leading,spacing:4)
                {
                    HStack(spacing: 3){
                        Text(trip.tripStatus ?? "")
                            .foregroundColor((trip.tripStatusValue ?? 0 == 3) ? Color.appRed : Color.appBlue )
                            .font(.robotoRegular(size: 14))
                        Text((trip.checkInDate ?? "").convertISODateTo(fromat: "dd MMM"))
                            .font(.robotoRegular(size: 14))
                    }
                    Text(trip.name ?? "")
                        .font(.robotoMedium(size: 14))
                        .lineLimit(1)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    Text((trip.bookedDate ?? "").convertISODateTo(fromat: "dd MMM yyyy"))
                        .font(.robotoRegular(size: 12))
                        .foregroundColor(.appGrey126)
                    HStack {
                        
                        IconWithTextMyTrips(image: AppIcons.perosn, text: trip.primaryGuest ?? "")
                        Spacer()
                        IconWithTextMyTrips(image: AppIcons.people, text: "\(trip.adultCount ?? 0) Members")
                    }
                }
                Image(AppIcons.arrowLeft)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 10)
                
            }
        }
        .padding(.all,7)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)
        
        
    }
    
} 
struct MyTripsHotelContainer: View {
    let trip:MyTrip
    var body: some View {
        VStack{
            HStack(spacing:10) {
                AppAsyncImage(url: trip.imageURL ?? "",width: 95,height:80,radius: 10)
                VStack(alignment: .leading,spacing:4)
                {
                    HStack(spacing: 3){
                        Text(trip.tripStatus ?? "")
                            .foregroundColor((trip.tripStatusValue ?? 0 == 3) ? Color.appRed : Color.appBlue )
                            .font(.robotoRegular(size: 14))
                        Text(
                            "\((trip.checkInDate ?? "").convertISODateTo(fromat: "dd MMM")) | \((trip.checkOutDate ?? "").convertISODateTo(fromat: "dd MMM"))")
                            .font(.robotoRegular(size: 14))
                    }
                    Text(trip.name ?? "")
                        .font(.robotoRegular(size: 14))
                        .lineLimit(1)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    Text((trip.bookedDate ?? "").convertISODateTo(fromat: "dd MMM yyyy"))
                        .font(.robotoRegular(size: 12))
                        .foregroundColor(.appGrey126)
                    HStack {
                        
                        IconWithTextMyTrips(image: AppIcons.perosn, text: trip.primaryGuest ?? "")
                        Spacer()
                        IconWithTextMyTrips(image: AppIcons.people, text: "\(trip.roomCount ?? 0) Rooms")
                    }
                }
                Image(AppIcons.arrowLeft)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 10)
                
            }
        }
        .padding(.all,7)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)

       
    }
}

struct MyTripsHolidayPackageContainer: View {
    let trip:MyTrip
    var body: some View {
        VStack{
            HStack(spacing:10) {
                AppAsyncImage(url: trip.imageURL ?? "",width: 95,height:80,radius: 10)
                VStack(alignment: .leading,spacing:4)
                {
                    HStack(spacing: 3){
                        Text(trip.tripStatus ?? "")
                            .foregroundColor((trip.tripStatusValue ?? 0 == 3) ? Color.appRed : Color.appBlue )
                            .font(.robotoRegular(size: 14))
                        Text(
                            "\((trip.checkInDate ?? "").convertISODateTo(fromat: "dd MMM")) | \((trip.checkOutDate ?? "").convertISODateTo(fromat: "dd MMM"))")
                            .font(.robotoRegular(size: 14))
                            .foregroundColor(.appBlack34)
                    }
                    Text(trip.name ?? "")
                        .font(.robotoRegular(size: 14))
                    
                        .lineLimit(1)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    Text((trip.bookedDate ?? "").convertISODateTo(fromat: "dd MMM yyyy"))
                        .font(.robotoRegular(size: 12))
                        .foregroundColor(.appGrey126)
                    HStack {
                        
                        IconWithTextMyTrips(image: AppIcons.perosn, text: trip.primaryGuest ?? "")
                        Spacer()
                        IconWithTextMyTrips(image: AppIcons.people, text: "\(trip.adultCount ?? 0) Rooms")
                    }
                }
                Image(AppIcons.arrowLeft)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 10)
                
            }
        }
        .padding(.all,7)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)

       
    }
}

struct MyTripsMeetUpContainer: View {
    let trip:MyTrip
    var body: some View {
        VStack{
            HStack(spacing:10) {
                AppAsyncImage(url: trip.imageURL ?? "",width: 95,height:80,radius: 10)
                VStack(alignment: .leading,spacing:4)
                {
                    HStack(spacing:3){
                        Text(trip.tripStatus ?? "")
                            .foregroundColor((trip.tripStatusValue ?? 0 == 3) ? Color.appRed : Color.appBlue )
                            .font(.robotoRegular(size: 14))
                        Text(
                            (trip.checkInDate ?? "").convertISODateTo(fromat: "dd MMM"))
                            .font(.robotoRegular(size: 14))
                    }
                    Text(trip.name ?? "")
                        .font(.robotoRegular(size: 14))
                        .lineLimit(1)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    Text((trip.bookedDate ?? "").convertISODateTo(fromat: "dd MMM yyyy"))
                        .font(.robotoRegular(size: 12))
                        .foregroundColor(.appGrey126)
                    HStack {
                        
                        IconWithTextMyTrips(image: AppIcons.perosn, text: trip.primaryGuest ?? "")
                        Spacer()
                        IconWithTextMyTrips(image: AppIcons.people, text: "\(trip.roomCount ?? 0) Rooms")
                    }
                }
                Image(AppIcons.arrowLeft)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 10)
                
            }
        }
        .padding(.all,7)
        .background(Color.appWhite)
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .shadow(color: .appBlack34.opacity(0.1),radius: 5,x: 2,y: 0)
        .padding(.horizontal,5)

       
    }
}



struct MyTipsBookingTypeSelector: View {
    @Binding var selectedBookingStatus:String
    let bookingStatus:[String]
    let onTap:()->Void
    
    var body: some View {
        Picker("",selection: $selectedBookingStatus)
        {
            ForEach(bookingStatus,id: \.self)
            {
                bookingStatus in
                Text(bookingStatus)
            }
        }
        .pickerStyle(.segmented)
        .onChange(of:selectedBookingStatus)
        {
            value in
           onTap()
        }
        .onAppear()
        {
            UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(Color.appBlue)
            UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            
            
        }
    }
}

//
//  profileView.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import SwiftUI

struct profileView: View {
    @StateObject var viewModel = ProfileViewModel()
    var body: some View {
        ScrollView{
            HStack
            {
                Image(systemName: "person.crop.circle")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100)
                VStack
                {
                    AppTextFieldRounderBorder(text: $viewModel.customerCode, placeHolder: "Enter Custome Code",header: "Customer Code", readOnly: true,width: 250)
                    AppTextFieldRounderBorder(text: $viewModel.customerName, placeHolder: "Enter Custome Name",header: "Customer Name", readOnly: false,width: 250)
                }
            }
            AppTextFieldRounderBorder(text: $viewModel.customerMobile, placeHolder: "Enter Mobile Number",header: "Mobile Number", readOnly: true)
            AppTextFieldRounderBorder(text: $viewModel.customerEmail, placeHolder: "Enter Email Id",header: "Email Id", readOnly: true)
            AppTextFieldRounderBorder(text: $viewModel.customerAddressLine1, placeHolder: "Enter Address Line 1",header: "Address Line 1", readOnly: false)
            AppTextFieldRounderBorder(text: $viewModel.customerAddressLine2, placeHolder: "Enter Address Line 2",header: "Address Line 2", readOnly: false)
            AppTextFieldRounderBorder(text: $viewModel.city, placeHolder: "Enter City",header: "City", readOnly: false)
            AppTextFieldRounderBorder(text: $viewModel.state, placeHolder: "Enter State",header: "State", readOnly: false)
            
            DropDownMenu(optionList: viewModel.countryList,
                         selecteOption: $viewModel.Country,
                         label:{
                option in
                return option?.name ?? ""
            }, header: "Country",onTap:{
                optin in
                print(optin)
            })
            
            
            AppTextFieldRounderBorder(text: $viewModel.pincode, placeHolder: "Enter Pin code",header: "Pin code", readOnly: false)
            DropDownMenu(optionList:genderList,selecteOption:$viewModel.slelctedGender
                         , label: {option in
                return option ?? "Selelct Gender"
            }, header: "Gender",onTap:{
                optin in
                print(optin)
            })
            DatePickerTextField(date: $viewModel.dateOfBirth, header: "DOB")
            Spacer().frame(height: 20)
            YellowButton(text: "Update") 
            BlueButton(text: "ChangeButton")
        }
        .padding(.horizontal,15)
        .task {
            await viewModel.getCountryList()
            await viewModel.getUserDetails()
        }
    }
}

#Preview {
    profileView()
}

//
//  profileViewModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import Foundation
class ProfileViewModel:ObservableObject
{
    @Published  var customerCode:String=""
    @Published  var customerName:String=""
    @Published  var customerMobile:String=""
    @Published  var customerEmail:String=""
    @Published  var customerAddressLine1:String=""
    @Published  var customerAddressLine2:String=""
    @Published  var city:String=""
    @Published  var state:String=""
    @Published  var Country:Country?
    @Published  var pincode:String=""
    @Published  var slelctedGender:String?
    @Published  var dateOfBirth:Date=Date()
    @Published var countryList:[Country]=[]
    
    
    func getUserDetails()async
    {
        do
        {
            var response: GetProfileResponseModel = try await ApiServices.getCustomerProfile(queryItems: [UserDefaultKeys.userId:UserDefaults.standard.string(forKey: UserDefaultKeys.userId) ?? "" ])
            customerCode = response.data?.customerCode ?? ""
            customerName = response.data?.customerName ?? ""
            customerMobile = response.data?.mobile ?? ""
            customerEmail = response.data?.email ?? ""
            customerAddressLine1 = response.data?.address1 ?? ""
            customerAddressLine2 = response.data?.address2 ?? ""
            city = response.data?.city ?? ""
            Country = countryList.first(where: {response.data?.country == $0.name})
            state = response.data?.state ?? ""
            pincode = response.data?.pincode ?? ""
            slelctedGender = response.data?.gender ?? ""
            dateOfBirth = ISO8601DateFormatter().date(from: (response.data?.dob ?? "")+"z") ?? Date()
        }
        catch
        {
            print(error)
        }
    }
    
    func getCountryList()async
    {
        do  {
            var response: GetCountryListResponseModel = try await ApiServices.getCountryList()
            countryList = response.data ?? []
        }
        catch
        {
            print(error)
        }}
}

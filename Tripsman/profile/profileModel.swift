//
//  profileModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getProfileResponseModel = try? JSONDecoder().decode(GetProfileResponseModel.self, from: jsonData)

import Foundation

// MARK: - GetProfileResponseModel
struct GetProfileResponseModel: Hashable, Codable {
    let totalRecords: Int?
    let data: ProfileData?
    let status: Int?
    let message: String?
}

// MARK: - DataClass
struct ProfileData: Hashable, Codable {
    let id: Int?
    let customerCode: String?
    let userId: String?
    let countryId: String?
    let customerName: String?
    let mobile: String?
    let email: String?
    let address1: String?
    let address2: String?
    let city: String?
    let state: String?
    let country: String?
    let pincode: String?
    let gender: String?
    let dob: String?
    let photo: String?
    let mobileOtpStatus: Int?
}

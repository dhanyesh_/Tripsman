//
//  appTextField.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 02/11/23.
//

import Foundation
import SwiftUI


struct PasswordTextField: View {
    @Binding  var text: String
    @State  var isSecured: Bool
    var width: CGFloat?=UIScreen.main.bounds.width
    var placeHolder: String
    var body: some View {
        ZStack(alignment: .trailing) {
            if isSecured {
                TextField("", text: $text,prompt: Text(placeHolder).foregroundColor(.appWhite))
                    .font(.robotoLight(size:16))
                    .foregroundColor(.appBlack34)
                    .frame(height: 20)
                   
            } else {
                SecureField(placeHolder, text: $text,prompt: Text(placeHolder).foregroundColor(.appWhite))
                    .font(.robotoLight(size:16))
                    .foregroundColor(.appBlack34)
                    .frame(height: 20)
            }
       
            Image(isSecured ? AppIcons.passwordShow : AppIcons.passwordHide)
                .resizable()
                .renderingMode(.template)
                .foregroundColor(Color.appWhite)
                .frame(width: 20,height: 20)
            
                .onTapGesture {
                    isSecured.toggle()
                }
            
        }
        .frame(width:width)
     
    }
}



struct AppTextField:View
{
    @Binding var text:String
    let placeHolder:String
    @State var errorText:String?=nil
    @State var readOnly:Bool?=false
    var width: CGFloat?=UIScreen.main.bounds.width

    var body: some View
    {
        
        VStack(alignment: .leading) {
            TextField("",text: $text,prompt: Text(placeHolder).foregroundColor((readOnly ?? false) ? .appGrey126 : .appWhite))
                .disabled(readOnly ?? false)
    
                .font(.robotoLight(size:16))
                .foregroundColor(.appBlack34)
                .frame(width: width, height: 20)
            .underlineBorder()
            Text(errorText ?? "")
                .font(.robotoLight(size: 12))
                .foregroundColor(.appRed)
        }
    }
}

struct AppTextFieldRounderBorder:View
{
    @Binding var text:String
    let placeHolder:String
    let header:String
    @State var errorText:String?=nil
    @State var readOnly:Bool?=false
    var width: CGFloat?=UIScreen.main.bounds.width-30

    var body: some View
    {
        
        VStack(alignment: .leading,spacing: 2) {
            Text(header)
                .font(.robotoLight(size: 12))
                .foregroundColor(.appBlack77)
            TextField("",text: $text,prompt: Text(placeHolder).foregroundColor(.appGrey126))
                .disabled(readOnly ?? false)
                .padding(.all,5)
                .font(.robotoLight(size:14))
                .foregroundColor(.appBlack34)
                .background(readOnly ?? false ? Color.appGrey239:Color.appWhite)
                
                .frame(width: width)
               
                .rounderBorder(cornerRadius: 5)
            Text(errorText ?? "")
                .font(.robotoLight(size: 12))
                .foregroundColor(.appRed)
        }
    }
}

struct SearchAppTextField:View {
    @Binding var text:String
    let placeHolder:String
    @State var errorText:String?=nil
    var width: CGFloat?=UIScreen.main.bounds.width
    var body: some View {
        HStack {
            TextField(placeHolder,text: $text)
                .font(.robotoRegular(size: 12))
            Image(systemName: "magnifyingglass")
        }
        .padding(.horizontal,10)
        .padding(.vertical,10)
    .rounderBorder(cornerRadius: 5)
        Text(errorText ?? "")
            .font(.robotoLight(size: 12))
            .foregroundColor(.appRed)
    }
}



struct DatePickerTextField:View {
    @State var isShowing:Bool = false
    @Binding var date:Date
    let header:String
    var body: some View {
        VStack(alignment: .leading,spacing: 4){
            Text(header)
                .font(.robotoRegular(size: 15))
            HStack{
                Text(date.formatTo(format: "dd-MM-yyyy"))
                Spacer()
                Image(systemName: "calendar")
                  
                    
                    
                
            }
            .padding(.vertical,5)
            .padding(.horizontal,10)
            .rounderBorder(cornerRadius: 5)
        }
        
                .onTapGesture {
                    isShowing.toggle()
                }
                .sheet(isPresented: $isShowing)
            {
                DatePicker("",selection: $date)
                    .datePickerStyle(.graphical)
                    .presentationDetents([.medium])
            }
        
    }
}




extension View {
    func underlineBorder() -> some View {
        self
            .padding(.top, 10)
            .overlay(Rectangle().frame(height: 1).padding(.top, 35))
            .foregroundColor(Color.appWhite)
            .padding(.top,10)
    }
    func rounderBorder(cornerRadius:Double) -> some View {
        self
            .overlay(RoundedRectangle(cornerRadius: cornerRadius)
                .stroke(Color.appGrey227,lineWidth: 1))
            .foregroundColor(Color.appGrey126)
            
    }
}



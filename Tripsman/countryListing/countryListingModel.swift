//
//  countryListingModel.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 16/11/23.
//

import Foundation
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getCountryListResponseModel = try? JSONDecoder().decode(GetCountryListResponseModel.self, from: jsonData)

import Foundation

// MARK: - GetCountryListResponseModel
struct GetCountryListResponseModel: Hashable, Codable {
    let totalRecords: Int?
    let data: [Country]?
    let status: Int?
    let message: String?
}

// MARK: - Datum
struct Country: Hashable, Codable {
    let countryId: Int?
    let name: String?
    let code: String?
    let icon: String?
}

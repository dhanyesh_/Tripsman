//
//  appButtons.swift
//  Tripsman
//
//  Created by Hexeam Software Solutions on 02/11/23.
//

import Foundation

import SwiftUI

struct BlueButton: View
{

    var text:String
    var width:CGFloat?=UIScreen.main.bounds.width-30
    var height:CGFloat?=38
    var radius:CGFloat? = nil


    var body: some View
    {
        Text(text)
            .font(.robotoMedium(size: 14))
            .frame(width: width,height: height)
            .foregroundColor(.white)
            .background(Color.appBlue.gradient)
            .cornerRadius(radius ?? 8)
    }
}

struct YellowButton: View
{

    var text:String
    var width:CGFloat?=UIScreen.main.bounds.width-30
    var height:CGFloat?=38
    var radius:CGFloat? = nil


    var body: some View
    {
        Text(text)
            .font(.robotoMedium(size: 14))
            .frame(width: width,height: height)
            .foregroundColor(.white)
            .background(Color.appYellow.gradient)
            .cornerRadius(radius ?? 8)
    }
}

struct GreyButton: View
{

    var text:String
    var width:CGFloat?=UIScreen.main.bounds.width
    var height:CGFloat?=38
    var radius:CGFloat? = nil


    var body: some View
    {
        Text(text)
            .font(.robotoMedium(size: 14))
            .frame(width: width,height: height)
            .foregroundColor(.white)
            .background(Color.appBlack34.opacity(0.5).gradient)
            .cornerRadius(radius ?? 8)
    }
}


struct EditButton:View {
    let width:CGFloat?
    init(width: CGFloat?=nil) {
        self.width = width
    }
    var body: some View {
        VStack {
            Image(systemName: "pencil")
                .resizable()
                .scaledToFit()
                .frame(width: width ?? 15)
                .foregroundColor(.appBlue)
            Text("Edit")
                .font(.robotoRegular(size: 10))
                .foregroundColor(.appBlue)
        }
    }
}

struct SearchButton:View {
    let width:CGFloat?
    init(width: CGFloat?=nil) {
        self.width = width
    }
    var body: some View {
        Image(systemName: "magnifyingglass")
            .resizable()
            .scaledToFit()
            .frame(width: width ?? 22)
            .foregroundColor(.appWhite)
            .padding(.vertical,10)
            .padding(.horizontal,15)
            .background(Color.appBlue.gradient)
            .cornerRadius(5)
    }
}


struct FilterByButton<Content: View>:View {
    var bottomSheet: Content
    var presentationDetent: Set<PresentationDetent>
    @State var showHolterFilters:Bool = false
    var body: some View {
        HStack{
            Text("Filter By")
            Spacer()
            Image(AppIcons.arrowDown)
                .resizable()
                .scaledToFit()
                .frame(width: 10)
        }.onTapGesture {
            showHolterFilters.toggle()
        }
        .sheet(isPresented:$showHolterFilters)
        {
          bottomSheet
                .presentationDetents(presentationDetent)
        }
        .padding(.horizontal,10)
        .padding(.vertical,5)
        .rounderBorder(cornerRadius: 5)
    }
}
